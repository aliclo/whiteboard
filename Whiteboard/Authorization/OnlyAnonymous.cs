﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Whiteboard.Authorization {

    public class OnlyAnonymousAttribute : TypeFilterAttribute {

        public OnlyAnonymousAttribute() : base(typeof(OnlyAnonymousFilter)) {

        }

    }

}
