﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Threading.Tasks;
using Whiteboard.Data;
using Whiteboard.Models.DataModels.Accounts;
using Whiteboard.Models.DataModels.Modules;
using Whiteboard.Repositories;

namespace Whiteboard {
    public static class AppDBInitializer {

        private const string WEB_APPS_CODE = "CS001";
        private const string WEB_APPS_NAME = "Web Apps";
        private const string WEB_APPS_LECTURER =
            AppUserDBInitializer.LECTURER_MEMBER1_USERNAME;

        private const string MOBILE_APPS_CODE = "CS002";
        private const string MOBILE_APPS_NAME = "Mobile Apps";
        private const string MOBILE_APPS_LECTURER =
            AppUserDBInitializer.LECTURER_MEMBER1_USERNAME;

        private const string EMBEDDED_SYSTEMS_CODE = "CS003";
        private const string EMBEDDED_SYSTEMS_NAME = "Embedded Systems";
        private const string EMBEDDED_SYSTEMS_LECTURER =
            AppUserDBInitializer.LECTURER_MEMBER2_USERNAME;

        private const string CRYPTOSYSTEMS_CODE = "CS004";
        private const string CRYPTOSYSTEMS_SYSTEMS_NAME = "Cryptosystems";
        private const string CRYPTOSYSTEMS_SYSTEMS_LECTURER =
            AppUserDBInitializer.LECTURER_MEMBER2_USERNAME;

        private static readonly ModuleCreation[] MODULE_CREATIONS = {
            new ModuleCreation(WEB_APPS_CODE, WEB_APPS_NAME,
                WEB_APPS_LECTURER),

            new ModuleCreation(MOBILE_APPS_CODE, MOBILE_APPS_NAME,
                MOBILE_APPS_LECTURER),

            new ModuleCreation(EMBEDDED_SYSTEMS_CODE, EMBEDDED_SYSTEMS_NAME,
                EMBEDDED_SYSTEMS_LECTURER),

            new ModuleCreation(CRYPTOSYSTEMS_CODE, CRYPTOSYSTEMS_SYSTEMS_NAME,
                CRYPTOSYSTEMS_SYSTEMS_LECTURER)
        };

        public static void AddSeededEntries(
                AppIdentityDbContext appIdentityDbContext,
                UserManager<AppUser> userManager,
                SignInManager<AppUser> signInManager) {

            ModuleRepository moduleRepository =
                new ModuleRepository(appIdentityDbContext);

            AppUserRepository appUserRepository = new AppUserRepository(
                appIdentityDbContext, userManager, signInManager);

            CreateModules(appUserRepository, moduleRepository, userManager);

            appIdentityDbContext.SaveChanges();
        }

        public static void RemoveSeededEntries(
            AppIdentityDbContext appIdentityDbContext) {

            foreach (ModuleCreation moduleCreation in MODULE_CREATIONS) {
                Module module = appIdentityDbContext.Modules.Find(
                    moduleCreation.Code);

                if (module != null && module.Seeded) {
                    module.RemoveReferences(appIdentityDbContext).Wait();

                    appIdentityDbContext.Modules.Remove(module);
                }
            }

            appIdentityDbContext.SaveChanges();
        }

        private static void CreateModules(AppUserRepository appUserRepository,
                ModuleRepository moduleRepository,
                UserManager<AppUser> userManager) {

            foreach(ModuleCreation moduleCreation in MODULE_CREATIONS) {
                CreateModule(appUserRepository, moduleRepository, userManager,
                    moduleCreation);
            }
        }

        private static void CreateModule(AppUserRepository appUserRepository,
                ModuleRepository moduleRepository,
                UserManager<AppUser> userManager,
                ModuleCreation moduleCreation) {

            Module module = new Module(moduleCreation.Code,
                moduleCreation.Name);

            bool newCreated = moduleRepository.SeedNonExistModuleAsync(
                module).Result;

            if (newCreated) {
                Task<AppUser> findLecturer = userManager.FindByNameAsync(
                    moduleCreation.Lecturer);

                Lecturer moduleLecturer = (Lecturer) findLecturer.Result;

                appUserRepository.AddModuleAsync(
                    moduleLecturer, module).Wait();
            }
        }

        private class ModuleCreation {

            public string Code { get; set; }
            public string Name { get; set; }
            public string Lecturer { get; set; }

            public ModuleCreation(string code, string name, string lecturer) {
                this.Code = code;
                this.Name = name;
                this.Lecturer = lecturer;
            }

        }

    }
}
