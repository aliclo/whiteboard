﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Whiteboard.Models;
using Whiteboard.Models.JoinModels;
using Whiteboard.Models.DataModels.Accounts;
using Whiteboard.Models.DataModels.Messaging;
using Whiteboard.Models.DataModels.Modules;

namespace Whiteboard.Data {
    public class AppIdentityDbContext : IdentityDbContext<AppUser> {
        public AppIdentityDbContext(
            DbContextOptions<AppIdentityDbContext> options) : base(options) {
        }

        public DbSet<AppUser> AppUsers { get; set; }

        public DbSet<Student> Students { get; set; }

        public DbSet<Lecturer> Lecturers { get; set; }

        public DbSet<Admin> Admins { get; set; }

        public DbSet<Developer> Developers { get; set; }

        public DbSet<Module> Modules { get; set; }

        public DbSet<UserModule> UserModules { get; set; }

        public DbSet<Message> Messages { get; set; }

        public DbSet<TextMessage> TextMessages { get; set; }

        public DbSet<MessageBoard> MessageBoards { get; set; }

        public DbSet<MsgBoardMember> MsgBoardMembers { get; set; }

        public DbSet<ModuleMessageBoard> ModuleMessageBoards { get; set; }

        public DbSet<Lecture> Lectures { get; set; }

        public DbSet<Resource> Resources { get; set; }

        public DbSet<UserSeenResource> UsersSeenResources { get; set; }

        protected override void OnModelCreating(ModelBuilder builder) {
            base.OnModelCreating(builder);

            builder.Entity<UserModule>()
                .HasKey(um => new { um.UserId, um.ModuleCode });

            builder.Entity<MsgBoardMember>()
                .HasKey(mbm => new { mbm.MessageBoardId, mbm.MemberId });

            builder.Entity<Message>()
                .HasKey(m => new { m.Id, m.MessageBoardId, m.SenderId });

            builder.Entity<ModuleMessageBoard>()
                .HasKey(mmb => new { mmb.MessageBoardId, mmb.ModuleCode });

            builder.Entity<Lecture>()
                .HasKey(l => new { l.ModuleCode, l.Id });

            builder.Entity<Resource>()
                .HasKey(r => new { r.ModuleCode, r.MaterialId, r.Id });

            builder.Entity<UserSeenResource>()
                .HasKey(usr => new { usr.UserId, usr.ModuleCode,
                    usr.MaterialId, usr.ResourceId });
        }

    }
}
