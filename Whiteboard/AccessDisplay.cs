﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Whiteboard {
    public class AccessDisplay {

        public const int HOME_ACCESS = 1;
        public const int MODULE_ACCESS = 2;

        public string Name { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public int Access { get; set; }

        public AccessDisplay(string name, string controller,
                string action, int access) {

            this.Name = name;
            this.Controller = controller;
            this.Action = action;
            this.Access = access;
        }

    }
}
