﻿using System;
using System.Collections.Generic;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Whiteboard.Data;
using Whiteboard.Models.DataModels.Modules;
using Whiteboard.Models.DataModels.Accounts;
using Whiteboard.Models.DataModels.Messaging;
using Whiteboard.Models.ViewModels.Messaging;
using Whiteboard.Repositories;
using Whiteboard.Models.ViewModels.Modules;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using System.IO;
using System.Net.Mime;

namespace Whiteboard.Controllers {
    [Authorize]
    public class ModuleController : Controller {

        private readonly AppIdentityDbContext _dbContext;
        private readonly ModuleRepository _moduleRepository;
        private readonly AppUserRepository _appUserRepository;
        private readonly MessageRepository _messageRepository;
        private readonly HtmlEncoder _htmlEncoder;
        private readonly JavaScriptEncoder _jsEncoder;

        public ModuleController(AppIdentityDbContext appIdentityDbContext,
                UserManager<AppUser> userManager,
                SignInManager<AppUser> signInManager,
                HtmlEncoder htmlEncoder, JavaScriptEncoder jsEncoder) {

            _dbContext = appIdentityDbContext;

            _moduleRepository = new ModuleRepository(appIdentityDbContext);

            _appUserRepository = new AppUserRepository(appIdentityDbContext,
                userManager, signInManager);

            _messageRepository = new MessageRepository(appIdentityDbContext);

            _htmlEncoder = htmlEncoder;
            _jsEncoder = jsEncoder;
        }

        [HttpGet]
        public async Task<IActionResult> Index(string code) {
            AccessResult accessResult = await AccessModule(code);

            if(accessResult.Redirect != null) {
                return accessResult.Redirect;
            }

            AppUser user = accessResult.User;
            Module module = accessResult.Module;

            ModuleHomeViewModel moduleHomeViewModel = new ModuleHomeViewModel(
                _jsEncoder, _htmlEncoder, module, user.AccessDisplays);

            return user.GetModuleIndexPage(this, moduleHomeViewModel);
        }

        [HttpGet]
        public async Task<IActionResult> Material(string code) {
            AccessResult accessResult = await AccessModule(code);

            if (accessResult.Redirect != null) {
                return accessResult.Redirect;
            }

            Module module = accessResult.Module;
            AppUser user = accessResult.User;

            ModuleMaterialViewModel moduleMaterialViewModel =
                new ModuleMaterialViewModel(_jsEncoder, _htmlEncoder, module,
                user.AccessDisplays);

            return await user.GetMaterialPage(this, _appUserRepository,
                moduleMaterialViewModel);
        }

        [Authorize(Roles = Roles.DEVELOPER)]
        [HttpGet]
        public async Task<IActionResult> MessagesDisplayTest() {
            AppUser john = await _appUserRepository.GetFromNameAsync(
                AppUserDBInitializer.LECTURER_MEMBER1_USERNAME);

            AppUser jim = await _appUserRepository.GetFromNameAsync(
                AppUserDBInitializer.STUDENT_CUSTOMER1_USERNAME);

            List<Message> messages = new List<Message>();

            TextMessage firstTextMessage = new TextMessage {
                Text = "Hello",
                Sender = john
            };

            TextMessage secondTextMessage = new TextMessage {
                Text = "Hi",
                Sender = jim
            };

            TextMessage thirdTextMessage = new TextMessage {
                Text = "How are you doing?",
                Sender = john
            };

            TextMessage fourthTextMessage = new TextMessage {
                Text = "Fine",
                Sender = jim
            };

            messages.Add(firstTextMessage);
            messages.Add(secondTextMessage);
            messages.Add(thirdTextMessage);
            messages.Add(fourthTextMessage);

            MessageBoard messageBoard = new MessageBoard {
                Messages = messages
            };

            MessageBoardViewModel messageBoardViewModel =
                new MessageBoardViewModel(_jsEncoder, _htmlEncoder,
                messageBoard);

            return View(messageBoardViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ReceiveMessage(
                int msgBoardID, string text) {

            MessageBoard messageBoard = await _messageRepository
                .GetMessageBoardAsync(msgBoardID);

            AppUser user = await _appUserRepository.GetAsync(User);

            if(messageBoard == null || text == null) {
                return BadRequest();
            }

            bool isMember = await _messageRepository.IsMemberAsync(
                user, messageBoard);

            if(!isMember) {
                return Unauthorized();
            }

            TextMessage textMessage = new TextMessage(
                user, DateTime.Now, text);

            await _messageRepository.AddMessageAsync(user, textMessage,
                messageBoard);

            string messageHtml = textMessage.GetDisplay(
                _jsEncoder,_htmlEncoder);

            return Content(messageHtml);
        }

        [Authorize(Policy = Claims.CAN_WRITE_MATERIAL)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddMaterial(string moduleCode,
                string name) {

            AccessResult accessResult = await AccessModule(moduleCode);

            if (accessResult.Redirect != null) {
                return Unauthorized();
            }

            Module module = accessResult.Module;
            Lecture lecture = new Lecture(name);

            await _moduleRepository.AddMaterialAsync(module, lecture);

            AddedMaterialViewModel addedMaterialViewModel =
                new AddedMaterialViewModel(lecture);

            return Json(addedMaterialViewModel);
        }

        [Authorize(Policy = Claims.CAN_WRITE_MATERIAL)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddMaterialItem() {
            IFormFile formFile = Request.Form.Files[0];

            StringValues moduleCodeValues;
            Request.Form.TryGetValue("moduleCode", out moduleCodeValues);

            StringValues materialIdValues;
            Request.Form.TryGetValue("materialId", out materialIdValues);

            StringValues nameValues;
            Request.Form.TryGetValue("name", out nameValues);

            string moduleCode = moduleCodeValues.ToString();
            long materialId = Convert.ToInt64(materialIdValues.ToString());
            string name = nameValues.ToString();

            AccessResult accessResult = await AccessModule(moduleCode);

            if (accessResult.Redirect != null) {
                return Unauthorized();
            }

            Lecture lecture = await _moduleRepository.GetMaterialAsync(
                moduleCode, materialId);

            if(lecture == null) {
                return NotFound();
            }

            byte[] file;

            using (MemoryStream stream = new MemoryStream()) {
                await formFile.CopyToAsync(stream);
                file = stream.ToArray();
            }

            Module module = accessResult.Module;

            Resource resource = new Resource(name, file, formFile.ContentType,
                formFile.FileName);

            await _moduleRepository.AddResourceAsync(module, lecture,
                resource);

            return Content(resource.Id.ToString());
        }

        [HttpGet]
        public async Task<IActionResult> DownloadResource(string moduleCode,
                long materialId, long itemId) {

            AccessResult accessResult = await AccessModule(moduleCode);

            if (accessResult.Redirect != null) {
                return Unauthorized();
            }

            AppUser user = accessResult.User;
            Module module = accessResult.Module;

            Lecture lecture = await _moduleRepository.GetMaterialAsync(
                moduleCode, materialId);

            if(lecture == null) {
                return BadRequest();
            }

            Resource resource = await _moduleRepository.GetResourceAsync(
                moduleCode, materialId, itemId);

            if (resource == null) {
                return BadRequest();
            }

            await user.SeenResource(_moduleRepository, module,
                lecture, resource);

            var cd = new ContentDisposition() {
                FileName = resource.FileName,
                Inline = true
            };

            Response.Headers.Append("Content-Disposition", cd.ToString());

            return File(resource.File, resource.FileContentType);
        }

        [Authorize(Policy = Claims.CAN_READ_ANALYTICS)]
        [HttpGet]
        public async Task<IActionResult> Analytics(string code) {
            AccessResult accessResult = await AccessModule(code);

            if (accessResult.Redirect != null) {
                return accessResult.Redirect;
            }

            AppUser user = accessResult.User;
            Module module = accessResult.Module;

            ModuleAnalyticsViewModel moduleAnalyticsViewModel =
                new ModuleAnalyticsViewModel(module, user.AccessDisplays);

            await moduleAnalyticsViewModel.InitialiseAsync(_dbContext, module);

            return View("Lecturer/Analytics", moduleAnalyticsViewModel);
        }

        private async Task<AccessResult> AccessModule(string code) {
            if (code == null) {
                IActionResult redirect = RedirectToAction("Index", "Home");
                return new AccessResult(redirect);
            }

            Module module = await _moduleRepository.GetModuleAsync(code);
            AppUser appUser = await _appUserRepository.GetAsync(User);

            if(module == null || appUser == null) {
                IActionResult redirect = RedirectToAction("Index", "Home");

                return new AccessResult(redirect);
            }

            bool hasAccess = await _moduleRepository.HasAccessToModuleAsync(
                appUser, module);

            if (hasAccess) {
                return new AccessResult(module, appUser);
            } else {
                IActionResult redirect = RedirectToAction(
                    "AccessDenied", "Account");

                return new AccessResult(redirect);
            }
        }

        private class AccessResult {

            public IActionResult Redirect { get; set; }
            public Module Module { get; set; }
            public AppUser User { get; set; }

            public AccessResult(IActionResult redirect) {
                this.Redirect = redirect;
            }

            public AccessResult(Module module, AppUser user) {
                this.Module = module;
                this.User = user;
            }

        }
    }
}