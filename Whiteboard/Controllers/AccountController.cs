﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Whiteboard.Authorization;
using Whiteboard.Data;
using Whiteboard.Models.DataModels.Accounts;
using Whiteboard.Models.ViewModels;
using Whiteboard.Models.ViewModels.Accounts;
using Whiteboard.Repositories;
using SignInResult = Microsoft.AspNetCore.Identity.SignInResult;

namespace Whiteboard.Controllers {

    [Authorize]
    public class AccountController : Controller {

        private const int GONE_STATUS_CODE = 410;

        private readonly AppUserRepository _appUserRepository;

        private readonly AppIdentityDbContext _dbContext;

        public AccountController(AppIdentityDbContext dbContext,
            UserManager<AppUser> userManager,
            SignInManager<AppUser> signInManager) {

            _appUserRepository = new AppUserRepository(dbContext, userManager,
                signInManager);

            this._dbContext = dbContext;
        }

        [HttpGet]
        public async Task<IActionResult> Logout() {
            await _appUserRepository.SignOutAsync();
            return RedirectToAction("Login");
        }

        [AllowAnonymous]
        [OnlyAnonymous]
        [HttpGet]
        public IActionResult Login() {
            return View();
        }

        [AllowAnonymous]
        [OnlyAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(UserLoginViewModel loginModel) {
            if (!ModelState.IsValid) {
                return View(loginModel);
            }

            SignInResult result = await _appUserRepository.SignInAsync(
                loginModel.Username, loginModel.Password, false);

            bool lockedOut = await _appUserRepository.IsLockedOutAsync(
                loginModel.Username);

            if(lockedOut) {
                ModelState.AddModelError("", "Account Locked");
                return View(loginModel);
            }

            if(result.Succeeded) {
                return RedirectToAction("Index", "Home");
            } else {
                ModelState.AddModelError("", "Failed to login");
                return View(loginModel);
            }
        }

        [Authorize(Policy = Claims.CAN_CREATE_STAFF)]
        [HttpGet]
        public async Task<IActionResult> StaffRegister() {
            AppUser user = await _appUserRepository.GetAsync(User);

            LecturerRegisterViewModel lecturerRegisterViewModel =
                new LecturerRegisterViewModel(user.AccessDisplays);

            return View(lecturerRegisterViewModel);
        }

        [Authorize(Policy = Claims.CAN_CREATE_STAFF)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> StaffRegister(
                LecturerRegisterViewModel regModel) {
            
            if (!ModelState.IsValid) {
                AppUser user = await _appUserRepository.GetAsync(User);
                regModel.UpdateAccessDisplays(user.AccessDisplays);

                return View(regModel);
            }

            Lecturer lecturer = new Lecturer(regModel);

            // Store the lecturer on the database.
            IdentityResult result = await _appUserRepository.CreateUserAsync(
                lecturer, regModel.Password);

            if (result.Succeeded) {
                return RedirectToAction("Index", "Home");
            } else {
                foreach (IdentityError error in result.Errors) {
                    ModelState.AddModelError("", error.Description);
                }

                AppUser user = await _appUserRepository.GetAsync(User);
                regModel.UpdateAccessDisplays(user.AccessDisplays);

                return View(regModel);
            }
        }

        [AllowAnonymous]
        [OnlyAnonymous]
        [HttpGet]
        public IActionResult StudentRegister() {
            return View();
        }

        [AllowAnonymous]
        [OnlyAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> StudentRegister(
            StudentRegisterViewModel regModel) {
            
            if (!ModelState.IsValid) {
                return View(regModel);
            }

            Student student = new Student(regModel);

            IdentityResult result = await _appUserRepository.CreateUserAsync(
                student, regModel.Password);

            if (result.Succeeded) {
                await _appUserRepository.SignInAsync(student, false);
                return RedirectToAction("Index", "Home");
            } else {
                foreach (IdentityError error in result.Errors) {
                    ModelState.AddModelError("", error.Description);
                }

                return View(regModel);
            }
        }

        [Authorize(Policy = Claims.CAN_MANAGE_EXISTING_ACCOUNTS)]
        [HttpGet]
        public async Task<IActionResult> AccountManagement() {
            List<Student> students =
                await _dbContext.Students.ToListAsync();

            List<Lecturer> lecturers =
                await _dbContext.Lecturers.ToListAsync();

            AppUser user = await _appUserRepository.GetAsync(User);

            AccountManagementViewModel accountManagementViewModel =
                new AccountManagementViewModel(user.AccessDisplays);

            await accountManagementViewModel.Initialise(
                _appUserRepository, students, lecturers);

            return View(accountManagementViewModel);
        }

        [Authorize(Policy = Claims.CAN_MANAGE_EXISTING_ACCOUNTS)]
        [HttpGet]
        public async Task<IActionResult> ManageAccount(string username) {
            AppUser user = await _appUserRepository.GetAsync(User);

            if (user == null) {
                return RedirectToAction("AccountManagement");
            }

            AppUser accountToManage =
                await _appUserRepository.GetFromNameAsync(username);

            ManageUserViewModel manageUserViewModel =
                new ManageUserViewModel(accountToManage, user.AccessDisplays);

            await manageUserViewModel.Initialise(_appUserRepository,
                accountToManage);

            return View(manageUserViewModel);
        }

        [Authorize(Policy = Claims.CAN_MANAGE_EXISTING_ACCOUNTS)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ManageAccount(
                ManageUserViewModel manageUserViewModel) {

            if (!ModelState.IsValid) {
                AppUser user = await _appUserRepository.GetFromNameAsync(
                    manageUserViewModel.Username);

                manageUserViewModel.UpdateAccessDisplays(user);

                return View(manageUserViewModel);
            }

            AppUser userToUpdate = await _appUserRepository.GetFromNameAsync(
                manageUserViewModel.Username);

            if (userToUpdate == null) {
                return RedirectToAction("AccountManagement");
            }

            IdentityResult result = await userToUpdate.Update(
                _appUserRepository, manageUserViewModel);

            if (result.Succeeded) {
                return RedirectToAction("AccountManagement");
            } else {
                foreach (IdentityError error in result.Errors) {
                    ModelState.AddModelError("", error.Description);
                }

                AppUser user = await _appUserRepository.GetFromNameAsync(
                    manageUserViewModel.Username);

                manageUserViewModel.UpdateAccessDisplays(user);

                return View(manageUserViewModel);
            }
        }

        [Authorize(Policy = Claims.CAN_MANAGE_EXISTING_ACCOUNTS)]
        [HttpDelete]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RemoveAccount(string username) {
            AppUser user = await _appUserRepository.GetFromNameAsync(username);

            if (user == null) {
                return StatusCode(GONE_STATUS_CODE);
            }

            await _appUserRepository.RemoveUserAsync(user);

            return Ok();
        }


        [HttpGet]
        public async Task<IActionResult> AccessDenied() {
            AppUser user = await _appUserRepository.GetAsync(User);

            EmptyAccessDisplayable emptyAccessDisplayable =
                new EmptyAccessDisplayable(user.AccessDisplays);

            return View(emptyAccessDisplayable);
        }
    }
}