﻿using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Whiteboard.Data;
using Whiteboard.Models;
using Whiteboard.Models.DataModels.Accounts;
using Whiteboard.Models.DataModels.Modules;
using Whiteboard.Repositories;

namespace Whiteboard.Controllers {

    [Authorize]
    public class HomeController : Controller {

        private readonly AppUserRepository _appUserRepository;
        private readonly ModuleRepository _moduleRepository;

        public HomeController(AppIdentityDbContext appIdentityDbContext,
                UserManager<AppUser> userManager,
                SignInManager<AppUser> signInManager) {

            _appUserRepository = new AppUserRepository(appIdentityDbContext,
                userManager, signInManager);

            _moduleRepository = new ModuleRepository(appIdentityDbContext);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddModule(string code) {
            AppUser user = await _appUserRepository.GetAsync(User);
            Module module = await _moduleRepository.GetModuleAsync(code);

            await _appUserRepository.AddModuleAsync(user, module);

            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> Index() {
            AppUser user = await _appUserRepository.GetAsync(User);

            return await user.GetHomepage(this, _moduleRepository);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None,
            NoStore = true)]
        public async Task<IActionResult> Error() {
            AppUser user = await _appUserRepository.GetAsync(User);

            ErrorViewModel errorViewModel = new ErrorViewModel(
                Activity.Current?.Id ?? HttpContext.TraceIdentifier,
                user.AccessDisplays);

            return View(errorViewModel);
        }
    }
}
