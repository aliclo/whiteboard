﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Threading.Tasks;
using Whiteboard.Repositories;
using Whiteboard.Models.DataModels.Accounts;

namespace Whiteboard {
    public static class AppUserDBInitializer {

        public const string LECTURER_MEMBER1_USERNAME = "Member1@email.com";
        public const string LECTURER_MEMBER2_USERNAME = "Member2@email.com";
        public const string ADMIN_MEMBER3_USERNAME = "Member3@email.com";

        public const string STUDENT_CUSTOMER1_USERNAME = "Customer1@email.com";
        public const string STUDENT_CUSTOMER2_USERNAME = "Customer2@email.com";
        public const string STUDENT_CUSTOMER3_USERNAME = "Customer3@email.com";
        public const string STUDENT_CUSTOMER4_USERNAME = "Customer4@email.com";
        public const string STUDENT_CUSTOMER5_USERNAME = "Customer5@email.com";

        public const string PASSWORD = "Password123!";

        private static readonly string[] USERNAMES = {
            LECTURER_MEMBER1_USERNAME, LECTURER_MEMBER2_USERNAME,
            ADMIN_MEMBER3_USERNAME, STUDENT_CUSTOMER1_USERNAME,
            STUDENT_CUSTOMER2_USERNAME, STUDENT_CUSTOMER3_USERNAME,
            STUDENT_CUSTOMER4_USERNAME, STUDENT_CUSTOMER5_USERNAME};

        public static void AddSeededEntries(UserManager<AppUser> userManager) {
            CreateDevelopers(userManager);
            CreateAdmins(userManager);
            CreateLecturers(userManager);
            CreateStudents(userManager);
        }

        public static void RemoveSeededEntries(
                UserManager<AppUser> userManager) {

            foreach (string username in USERNAMES) {
                Task<AppUser> findUser = userManager.FindByNameAsync(username);
                AppUser user = findUser.Result;

                if (user != null && user.Seeded) {
                    userManager.DeleteAsync(user).Wait();
                }
            }
        }

        public static void CreateRoles(RoleManager<IdentityRole> roleManager) {
            foreach(string role in Roles.ROLESARR) {
                CreateNonExistRole(roleManager, role);
            }
        }

        private static void CreateDevelopers(
                UserManager<AppUser> userManager) {

        }

        private static void CreateAdmins(UserManager<AppUser> userManager) {
            Admin member3 = new Admin {
                UserName = ADMIN_MEMBER3_USERNAME,
                Forename = "Member3",
                Surname = "Test",
                Email = ADMIN_MEMBER3_USERNAME,
            };

            SeedUser(userManager, member3, PASSWORD);
        }

        private static void CreateLecturers(UserManager<AppUser> userManager) {
            Lecturer member1 = new Lecturer {
                UserName = LECTURER_MEMBER1_USERNAME,
                Forename = "Member1",
                Surname = "Test",
                Email = LECTURER_MEMBER1_USERNAME,
            };

            SeedUser(userManager, member1, PASSWORD);

            Lecturer member2 = new Lecturer {
                UserName = LECTURER_MEMBER2_USERNAME,
                Forename = "Member2",
                Surname = "Test",
                Email = LECTURER_MEMBER2_USERNAME,
            };

            SeedUser(userManager, member2, PASSWORD);
        }

        private static void CreateStudents(UserManager<AppUser> userManager) {
            Student customer1 = new Student {
                UserName = STUDENT_CUSTOMER1_USERNAME,
                Forename = "Customer1",
                Surname = "Test",
                Email = STUDENT_CUSTOMER1_USERNAME,
            };

            SeedUser(userManager, customer1, PASSWORD);

            Student customer2 = new Student {
                UserName = STUDENT_CUSTOMER2_USERNAME,
                Forename = "Customer2",
                Surname = "Test",
                Email = STUDENT_CUSTOMER2_USERNAME,
            };

            SeedUser(userManager, customer2, PASSWORD);

            Student customer3 = new Student {
                UserName = STUDENT_CUSTOMER3_USERNAME,
                Forename = "Customer3",
                Surname = "Test",
                Email = STUDENT_CUSTOMER3_USERNAME,
            };

            SeedUser(userManager, customer3, PASSWORD);

            Student customer4 = new Student {
                UserName = STUDENT_CUSTOMER4_USERNAME,
                Forename = "Customer4",
                Surname = "Test",
                Email = STUDENT_CUSTOMER4_USERNAME,
            };

            SeedUser(userManager, customer4, PASSWORD);

            Student customer5 = new Student {
                UserName = STUDENT_CUSTOMER5_USERNAME,
                Forename = "Customer5",
                Surname = "Test",
                Email = STUDENT_CUSTOMER5_USERNAME,
            };

            SeedUser(userManager, customer5, PASSWORD);
        }

        private static void SeedUser(UserManager<AppUser> userManager,
                AppUser user, string password) {

            user.Seeded = true;
            AppUserRepository.CreateNonExistUserAsync(
                userManager, user, password).Wait();
        }

        private static void CreateNonExistRole(
                RoleManager<IdentityRole> roleManager, string role) {

            Task<bool> roleExistsTask = roleManager.RoleExistsAsync(role);
            bool roleExists = roleExistsTask.Result;

            if (!roleExists) {
                Task<IdentityResult> createRoleTask = roleManager.CreateAsync(
                    new IdentityRole(role));

                IdentityResult result = createRoleTask.Result;

                if (result.Succeeded) {
                    Console.WriteLine("Successfully created role: " + role);
                } else {
                    Console.WriteLine("Failed to create role: " + role);
                }
            }
        }

    }
}
