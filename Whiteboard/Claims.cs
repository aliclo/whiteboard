﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Whiteboard {
    public static class Claims {

        public enum CLAIMS { CAN_CREATE_STAFF,
            CAN_MANAGE_EXISTING_ACCOUNTS, CAN_WRITE_MATERIAL,
            CAN_READ_ANALYTICS };

        public const string CAN_CREATE_STAFF = "CanCreateStaff";

        public const string CAN_MANAGE_EXISTING_ACCOUNTS =
            "CanManageExistingAccounts";

        public const string CAN_WRITE_MATERIAL = "CanWriteMaterial";

        public const string CAN_READ_ANALYTICS = "CanReadAnalytics";

        public static readonly string[] CLAIMSARR = { CAN_CREATE_STAFF,
            CAN_MANAGE_EXISTING_ACCOUNTS, CAN_WRITE_MATERIAL,
            CAN_READ_ANALYTICS };

        public static readonly AccessDisplay[] CLAIMS_ACCESS = {
            new AccessDisplay("Register Staff", "Account", "StaffRegister",
                AccessDisplay.HOME_ACCESS),

            new AccessDisplay("Manage Accounts", "Account",
                "AccountManagement", AccessDisplay.HOME_ACCESS),

            null,

            new AccessDisplay("Analytics", "Module", "Analytics",
                AccessDisplay.MODULE_ACCESS)
        };

        public static readonly Dictionary<string, CLAIMS> CLAIMS_ORDERING
            = new Dictionary<string, CLAIMS>();

        static Claims() {
            for (int i = 0; i < CLAIMSARR.Length; i++) {
                string claim = CLAIMSARR[i];
                CLAIMS_ORDERING[claim] = (CLAIMS) i;
            }
        }

        public static string GetClaimStr(CLAIMS claim) {
            return CLAIMSARR[(int)claim];
        }

        public static string[] GetClaimNames(CLAIMS[] claims) {
            string[] claimNames = new string[claims.Length];

            for (int i = 0; i < claims.Length; i++) {
                claimNames[i] = GetClaimStr(claims[i]);
            }

            return claimNames;
        }

    }
}
