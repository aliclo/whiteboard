﻿function htmlEncode(html) {
    return html
        .replace(/&/g, "&amp;")
        .replace(/"/g, "&quot;")
        .replace(/'/g, "&#39;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;")
        .replace("\n", "</br>");
}

/*function GetMessageContents(username, dateTime, contents) {
    var header = "<b class='msgheader'>" + username
        + "</b></br>";

    var footer = "<i class='msgfooter'></br>"
        + dateTime + "</i></br>";

    return "<p class='message'>" + header + contents
        + footer + "</p>";
}

function GetTextMsgContents(username, dateTime, text) {
    var contents = htmlEncode(text);
    return GetMessageCpontents(username, dateTime, contents);
}*/