﻿function setupHandlers(messageReceiveUrl) {
    $(document).on("click", ".messagebutton", function () {
        var messenger = $(this).parent().parent();
        var msgTextBox = messenger.find(".messagetextbox");
        var messagesDisplay = messenger.find(".messages-display");
        var messageBoardId = messenger.attr("data-message-board-id");

        $.ajax({
            type: "POST",
            url: messageReceiveUrl,
            data: AntiForgeryToken({ msgBoardID: messageBoardId, text: msgTextBox.val() }),
            success: function (msgContents) {
                messagesDisplay.append(msgContents);
                msgTextBox.val("");
                messagesDisplay.scrollTop(
                    messagesDisplay.prop('scrollHeight'));
            }
        });
    });
}

function messenger(messageBoardId, messages) {
    return "<div id='messenger-" + messageBoardId + "' data-message-board-id='" + messageBoardId + "'>" +
        "   <div class='messages-display'>" +
        "       " + messages +
        "   </div>" +
        "   <div class='message-input'>" +
        "       <textarea class='messagetextbox'></textarea>" +
        "       <button class='messagebutton'>Send</button>" +
        "   </div>" +
        "</div>";
}

function createMessenger(messageBoardId, messages) {
    document.write(messenger(messageBoardId, messages));
}

function createMessengerIn(elem, messageBoardId, messages) {
    elem.innerHTML += messenger(messageBoardId, messages);
}