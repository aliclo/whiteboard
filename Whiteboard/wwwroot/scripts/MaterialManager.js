﻿function createViewableMaterial(name, id) {
    var textName = htmlEncode(name);

    return "<div class='material-display table-material' id='" + id + "'>" +
        "   <div class='row-field'>" +
        "       <div class='material-header cell-field'>" +
        "           <p class='material-title'>" + textName + "</p>" +
        "       </div>" +
        "       <div class='material-footer cell-field'>" +
        "           <div class='files-display'>" +
        "               <div class='table-files'>" +
        "                   <div class='row-field file-icons'>" +
        "                   </div>" +
        "                   <div class='row-field file-names'>" +
        "                   </div>" +
        "               </div>" +
        "           </div>" +
        "       </div>" +
        "   </div>" +
        "</div>";
}

function createEditableMaterial(name, id) {
    var textName = htmlEncode(name);

    return "<div class='material-display table-material' id='" + id + "'>" +
        "   <div class='row-field'>" +
        "       <div class='material-header cell-field'>" +
        "           <p class='material-title'>" + textName + "</p>" +
        "       </div>" +
        "       <div class='material-footer cell-field'>" +
        "           <div class='files-display'>" +
        "               <div class='table-files'>" +
        "                   <div class='row-field file-icons'>" +
        "                       <div class='add-item-icon cell-field'></div>" +
        "                   </div>" +
        "                   <div class='row-field file-names'>" +
        "                       <div class='add-item-name cell-field'></div>" +
        "                   </div>" +
        "               </div>" +
        "           </div>" +
        "       </div>" +
        "   </div>" +
        "</div>";
}

function createItemFileIcon(moduleCode, materialId, itemId, fileIcon) {
    return "<a class='cell-field file-image' href='DownloadResource?moduleCode=" + moduleCode + "&materialId=" + materialId + "&itemId=" + itemId + "'>" +
        "   <img src='" + fileIcon + "' alt='File' />" +
        "</a>";
}

function createItemName(name) {
    var textName = htmlEncode(name);

    return "<p class='cell-field file-name'>" + textName + "</p>";
}