﻿function GetForgeryToken() {
    var loc = "#__AntiForgeryForm input[name = __RequestVerificationToken]";
    var token = $(loc).val();

    return token;
}

function AntiForgeryToken(data) {
    var token = GetForgeryToken();
    data.__RequestVerificationToken = token;
    return data;
}

function AppendForgeryToken(fileData) {
    fileData.append("__RequestVerificationToken", GetForgeryToken());
}