﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Whiteboard {
    public static class Roles {

        public const string LECTURER = "Lecturer";
        public const string STUDENT = "Student";
        public const string ADMIN = "Admin";
        public const string DEVELOPER = "Developer";

        public static readonly string[] ROLESARR = {STUDENT, LECTURER, ADMIN,
            DEVELOPER };

    }
}
