﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Whiteboard.Models.DataModels.Accounts;
using Whiteboard.Models.DataModels.Modules;

namespace Whiteboard.Models.JoinModels {
    public class UserSeenResource {

        public string UserId { get; set; }
        public AppUser User { get; set; }

        public string ModuleCode { get; set; }
        public Module Module { get; set; }

        public long MaterialId { get; set; }
        public Lecture Material { get; set; }

        public long ResourceId { get; set; }
        public Resource Resource { get; set; }

        public UserSeenResource() {

        }

        public UserSeenResource(AppUser user, Module module,
                Lecture material, Resource resource) {

            this.UserId = user.Id;
            this.User = user;

            this.ModuleCode = module.Code;
            this.Module = module;

            this.MaterialId = material.Id;
            this.Material = Material;

            this.ResourceId = resource.Id;
            this.Resource = resource;
        }

    }
}
