﻿using Whiteboard.Models.DataModels.Accounts;
using Whiteboard.Models.DataModels.Modules;

namespace Whiteboard.Models.JoinModels {
    public class UserModule {

        public string UserId { get; set; }
        public AppUser User { get; set; }

        public string ModuleCode { get; set; }
        public Module Module { get; set; }

        public UserModule() {

        }

        public UserModule(AppUser user, Module module) {
            this.UserId = user.Id;
            this.User = user;

            this.ModuleCode = module.Code;
            this.Module = module;
        }

    }
}
