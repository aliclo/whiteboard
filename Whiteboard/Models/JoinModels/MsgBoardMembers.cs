﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Whiteboard.Models.DataModels.Accounts;
using Whiteboard.Models.DataModels.Messaging;

namespace Whiteboard.Models.JoinModels {
    public class MsgBoardMember {

        public long MessageBoardId { get; set; }
        public MessageBoard MessageBoard { get; set; }

        public string MemberId { get; set; }
        public AppUser Member { get; set; }

        public MsgBoardMember() {

        }

        public MsgBoardMember(MessageBoard messageBoard, AppUser member) {
            this.MessageBoardId = messageBoard.Id;
            this.MessageBoard = messageBoard;

            this.MemberId = member.Id;
            this.Member = member;
        }

    }
}
