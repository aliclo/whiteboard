﻿using Whiteboard.Models.DataModels.Messaging;
using Whiteboard.Models.DataModels.Modules;

namespace Whiteboard.Models.JoinModels {
    public class ModuleMessageBoard {

        public string ModuleCode { get; set; }
        public Module Module { get; set; }

        public long MessageBoardId { get; set; }
        public MessageBoard MessageBoard { get; set; }

        public ModuleMessageBoard() {

        }

        public ModuleMessageBoard(Module module, MessageBoard messageBoard) {
            this.ModuleCode = module.Code;
            this.Module = module;

            this.MessageBoardId = messageBoard.Id;
            this.MessageBoard = messageBoard;
        }

    }
}
