﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Whiteboard.Models.ViewModels.Accounts {
    public class StudentRegisterViewModel : UserRegisterViewModel {

        private static readonly List<AccessDisplay> ACCESS_DISPLAYS =
            new List<AccessDisplay>();

        public override List<AccessDisplay> GetAccessDisplays() {
            return ACCESS_DISPLAYS;
        }

    }
}
