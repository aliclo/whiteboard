﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Whiteboard.Models.ViewModels.Accounts {
    public abstract class UserRegisterViewModel : IAccessDisplayable {
        [Required]
        [MinLength(2), MaxLength(30), DataType(DataType.Text)]
        [Display(Name = "Username")]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email Address")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Confirm Email Address")]
        [Compare("Email", ErrorMessage = "Both email addresses don't match")]
        public string ConfirmEmail { get; set; }

        [Required]
        [MinLength(2), MaxLength(30), DataType(DataType.Text)]
        [Display(Name = "Forename")]
        public string Forename { get; set; }

        [Required]
        [MinLength(2), MaxLength(30), DataType(DataType.Text)]
        [Display(Name = "Surname")]
        public string Surname { get; set; }

        [Required]
        [MinLength(2), MaxLength(75), DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required]
        [MinLength(2), MaxLength(75), DataType(DataType.Password)]
        [Display(Name = "Confirm Password")]
        [Compare("Password", ErrorMessage = "Both passwords don't match")]
        public string ConfirmPassword { get; set; }

        public abstract List<AccessDisplay> GetAccessDisplays();
    }
}
