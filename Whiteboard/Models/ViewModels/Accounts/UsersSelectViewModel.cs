﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Whiteboard.Models.DataModels.Accounts;
using Whiteboard.Repositories;
using static Whiteboard.Claims;

namespace Whiteboard.Models.ViewModels.Accounts {
    public class UsersSelectViewModel {

        public string[] AvailableClaims { get; set; }
        public List<UserSelectViewModel> Users { get; set; }

        public UsersSelectViewModel(CLAIMS[] availableClaims,
                List<UserSelectViewModel> users) {

            AvailableClaims = GetClaimNames(availableClaims);

            this.Users = users;
        }

    }
}
