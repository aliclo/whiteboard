﻿using System.Collections.Generic;
using Whiteboard.Models.DataModels.Accounts;
using Whiteboard.Models.DataModels.Modules;

namespace Whiteboard.Models.ViewModels.Accounts {
    public class LecturerStartViewModel : UserStartViewModel {

        public string Title { get; set; }
        public ModulesViewModel Modules { get; set; }

        public LecturerStartViewModel(Lecturer lecturer,
                List<Module> availableModules) : base(lecturer) {

            Title = lecturer.Title;
            Modules = new ModulesViewModel(lecturer.Modules, availableModules);
        }

    }
}
