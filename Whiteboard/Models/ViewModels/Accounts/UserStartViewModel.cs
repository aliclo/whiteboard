﻿using System.Collections.Generic;
using Whiteboard.Models.DataModels.Accounts;

namespace Whiteboard.Models.ViewModels.Accounts {
    public class UserStartViewModel : IAccessDisplayable {

        public string Forename { get; set; }
        private readonly List<AccessDisplay> _accessDisplays;

        public UserStartViewModel(AppUser user) {

            Forename = user.Forename;
            _accessDisplays = user.AccessDisplays;
        }

        public List<AccessDisplay> GetAccessDisplays() {
            return _accessDisplays;
        }

    }
}
