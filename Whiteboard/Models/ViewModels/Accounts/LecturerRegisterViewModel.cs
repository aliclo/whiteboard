﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Whiteboard.Models.ViewModels.Accounts {
    public class LecturerRegisterViewModel : UserRegisterViewModel {

        [Required]
        [MinLength(1), MaxLength(10), DataType(DataType.Text)]
        [Display(Name = "Title")]
        public string Title { get; set; }

        private List<AccessDisplay> _accessDisplays;

        public LecturerRegisterViewModel() : base() {

        }

        public LecturerRegisterViewModel(List<AccessDisplay> accessDisplays)
                : base() {

            this._accessDisplays = accessDisplays;
        }

        public override List<AccessDisplay> GetAccessDisplays() {
            return _accessDisplays;
        }

        public void UpdateAccessDisplays(List<AccessDisplay> accessDisplays) {
            this._accessDisplays = accessDisplays;
        }

    }
}
