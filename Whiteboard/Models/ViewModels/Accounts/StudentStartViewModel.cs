﻿using System.Collections.Generic;
using Whiteboard.Models.DataModels.Accounts;
using Whiteboard.Models.DataModels.Modules;

namespace Whiteboard.Models.ViewModels.Accounts {
    public class StudentStartViewModel : UserStartViewModel {

        public ModulesViewModel Modules { get; set; }

        public StudentStartViewModel(Student student,
                List<Module> availableModules) : base(student) {

            this.Modules = new ModulesViewModel(student.Modules,
                availableModules);
        }

    }
}
