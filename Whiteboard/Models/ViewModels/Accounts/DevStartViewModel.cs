﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Whiteboard.Models.DataModels.Accounts;

namespace Whiteboard.Models.ViewModels.Accounts {
    public class DevStartViewModel : UserStartViewModel {

        public DevStartViewModel(Developer developer) : base(developer) {

        }

    }
}
