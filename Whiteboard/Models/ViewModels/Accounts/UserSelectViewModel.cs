﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Whiteboard.Models.DataModels.Accounts;
using Whiteboard.Repositories;

namespace Whiteboard.Models.ViewModels.Accounts {
    public class UserSelectViewModel {

        public string Username { get; set; }
        public string[] Claims { get; set; }

        public UserSelectViewModel(AppUser appUser) {
            this.Username = appUser.UserName;
        }

        public async Task Initialise(AppUserRepository appUserRepository,
                AppUser appUser) {

            bool[] claimsBool =
                await appUserRepository.GetClaimsAsync(appUser);

            Claims = new string[claimsBool.Length];

            for(int i = 0; i < Claims.Length; i++) {
                if(claimsBool[i]) {
                    Claims[i] = "yes";
                } else {
                    Claims[i] = "no";
                }
            }
        }

    }
}
