﻿using System.Collections.Generic;
using Whiteboard.Models.DataModels.Modules;

namespace Whiteboard.Models.ViewModels.Accounts {
    public class ModulesViewModel {

        public ICollection<ModuleStartViewModel> TakenModules { get; set; }
        public ICollection<ModuleStartViewModel> AvailableModules { get; set; }

        public ModulesViewModel(List<Module> takenModules,
            List<Module> availableModules) {

            TakenModules = new List<ModuleStartViewModel>();

            foreach(Module module in takenModules) {
                TakenModules.Add(new ModuleStartViewModel(module));
            }

            AvailableModules = new List<ModuleStartViewModel>();

            foreach (Module module in availableModules) {
                AvailableModules.Add(new ModuleStartViewModel(module));
            }
        }

    }
}
