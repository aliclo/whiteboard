﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Whiteboard.Models.DataModels.Modules;

namespace Whiteboard.Models.ViewModels.Accounts {
    public class ModuleStartViewModel {

        public string Code { get; set; }

        public string Name { get; set; }

        public ModuleStartViewModel(Module module) {
            this.Code = module.Code;
            this.Name = module.Name;
        }

    }
}
