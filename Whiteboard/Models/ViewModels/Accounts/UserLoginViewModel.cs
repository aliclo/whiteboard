﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Whiteboard.Models.ViewModels.Accounts {
    public class UserLoginViewModel : IAccessDisplayable {

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Username")]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        private static readonly List<AccessDisplay> ACCESS_DISPLAY =
            new List<AccessDisplay>();

        public List<AccessDisplay> GetAccessDisplays() {
            return ACCESS_DISPLAY;
        }

    }
}
