﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Whiteboard.Models.DataModels.Accounts;
using Whiteboard.Repositories;
using static Whiteboard.Claims;

namespace Whiteboard.Models.ViewModels.Accounts {
    public class ManageUserViewModel : IAccessDisplayable {

        public string Username { get; set; }

        [Required]
        [MinLength(2), MaxLength(30), DataType(DataType.Text)]
        public string Forename { get; set; }

        [Required]
        [MinLength(2), MaxLength(30), DataType(DataType.Text)]
        public string Surname { get; set; }

        public bool[] Claims { get; set; }

        public string[] AvailableClaims { get; set; }

        private List<AccessDisplay> _accessDisplays;

        public ManageUserViewModel() {
            
        }

        public ManageUserViewModel(AppUser user,
                List<AccessDisplay> accessDisplays) {

            this.Username = user.UserName;
            this.Forename = user.Forename;
            this.Surname = user.Surname;

            AvailableClaims = GetClaimNames(user.GetAvailableClaims());

            _accessDisplays = accessDisplays;
        }

        public async Task Initialise(AppUserRepository appUserRepository,
                AppUser user) {

            Claims = await appUserRepository.GetClaimsAsync(user);
        }

        public List<AccessDisplay> GetAccessDisplays() {
            return _accessDisplays;
        }

        public void UpdateAccessDisplays(AppUser user) {
            AvailableClaims = GetClaimNames(user.GetAvailableClaims());
            this._accessDisplays = user.AccessDisplays;
        }

    }
}
