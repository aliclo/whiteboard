﻿using Whiteboard.Models.DataModels.Accounts;

namespace Whiteboard.Models.ViewModels.Accounts {
    public class AdminStartViewModel : UserStartViewModel {

        public AdminStartViewModel(Admin admin) : base(admin) {
            
        }

    }
}
