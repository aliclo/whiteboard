﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Whiteboard.Models.DataModels.Accounts;
using Whiteboard.Repositories;

namespace Whiteboard.Models.ViewModels.Accounts {
    public class AccountManagementViewModel : IAccessDisplayable {

        public UsersSelectViewModel Students { get; set; }
        public UsersSelectViewModel Lecturers { get; set; }

        private readonly List<AccessDisplay> _accessDisplays;

        public AccountManagementViewModel(List<AccessDisplay> accessDisplays) {
            _accessDisplays = accessDisplays;
        }

        public async Task Initialise(AppUserRepository appUserRepository,
                List<Student> students, List<Lecturer> lecturers) {

            List<UserSelectViewModel> studentsList =
                new List<UserSelectViewModel>();

            foreach (Student student in students) {
                UserSelectViewModel userSelectViewModel =
                    new UserSelectViewModel(student);

                await userSelectViewModel.Initialise(
                    appUserRepository, student);

                studentsList.Add(userSelectViewModel);
            }

            Students = new UsersSelectViewModel(Student.AVAILABLE_CLAIMS,
                studentsList);

            List<UserSelectViewModel> lecturersList =
                new List<UserSelectViewModel>();

            foreach (Lecturer lecturer in lecturers) {
                UserSelectViewModel userSelectViewModel =
                    new UserSelectViewModel(lecturer);

                await userSelectViewModel.Initialise(
                    appUserRepository, lecturer);

                lecturersList.Add(userSelectViewModel);
            }

            Lecturers = new UsersSelectViewModel(Lecturer.AVAILABLE_CLAIMS,
                lecturersList);
        }

        public List<AccessDisplay> GetAccessDisplays() {
            return _accessDisplays;
        }

    }
}
