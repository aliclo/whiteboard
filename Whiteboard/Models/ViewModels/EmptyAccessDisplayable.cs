﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Whiteboard.Models.ViewModels {
    public class EmptyAccessDisplayable : IAccessDisplayable {

        private readonly List<AccessDisplay> _accessDisplays;

        public EmptyAccessDisplayable(List<AccessDisplay> accessDisplays) {
            this._accessDisplays = accessDisplays;
        }

        public List<AccessDisplay> GetAccessDisplays() {
            return _accessDisplays;
        }

    }
}
