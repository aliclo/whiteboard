﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Whiteboard.Models.ViewModels {
    public interface IAccessDisplayable {

        List<AccessDisplay> GetAccessDisplays();

    }
}
