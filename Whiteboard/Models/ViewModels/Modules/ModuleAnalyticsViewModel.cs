﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Whiteboard.Data;
using Whiteboard.Models.DataModels.Accounts;
using Whiteboard.Models.DataModels.Modules;
using Whiteboard.Models.JoinModels;

namespace Whiteboard.Models.ViewModels.Modules {
    public class ModuleAnalyticsViewModel : IAccessDisplayable {

        private const int MAX_PERCENT = 100;

        public string Code { get; set; }
        public string Name { get; set; }
        public string TotalSeen { get; set; }
        public float TotalSeenNum { get; set; }

        public List<MaterialAnalyticsViewModel> MaterialAnalytics { get; set; }

        private readonly List<AccessDisplay> _accessDisplays;

        public ModuleAnalyticsViewModel(Module module,
                List<AccessDisplay> accessDisplays) {

            this.Code = module.Code;
            this.Name = module.Name;
            this._accessDisplays = accessDisplays;
        }

        public async Task InitialiseAsync(AppIdentityDbContext dbContext,
                Module module) {

            IQueryable<UserModule> usersModule = dbContext.UserModules.Where(
                um => um.ModuleCode.Equals(Code));

            int studentsNum = await dbContext.Students.Where(
                u => usersModule.Any(um => um.UserId.Equals(u.Id)))
                .CountAsync();

            MaterialAnalytics = new List<MaterialAnalyticsViewModel>();

            if (studentsNum == 0) {
                GetLecturesNoStudents(module.Lectures);
                TotalSeen = "No students";
            } else {
                List<Lecture> lectures = module.Lectures;

                if (lectures.Count() == 0) {
                    TotalSeenNum = MAX_PERCENT;
                    TotalSeen = "No material";
                } else {
                    await GetLectures(dbContext, studentsNum, lectures);
                }
            }
        }

        public List<AccessDisplay> GetAccessDisplays() {
            return _accessDisplays;
        }

        private async Task GetLectures(AppIdentityDbContext dbContext,
                int studentsNum, List<Lecture> lectures) {

            foreach (Lecture lecture in lectures) {
                MaterialAnalyticsViewModel materialAnalytics =
                    new MaterialAnalyticsViewModel(lecture);

                await materialAnalytics.InitialiseAsync(dbContext,
                    studentsNum, lecture);

                TotalSeenNum += materialAnalytics.TotalSeenNum;

                MaterialAnalytics.Add(materialAnalytics);
            }

            TotalSeenNum = TotalSeenNum / lectures.Count();
            TotalSeen = TotalSeenNum.ToString("n2") + "%";
        }

        private void GetLecturesNoStudents(List<Lecture> lectures) {
            foreach (Lecture lecture in lectures) {
                MaterialAnalyticsViewModel materialAnalytics =
                    new MaterialAnalyticsViewModel(lecture, false);

                MaterialAnalytics.Add(materialAnalytics);
            }
        }

    }
}
