﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Whiteboard.Models.DataModels.Modules;

namespace Whiteboard.Models.ViewModels.Modules {
    public class ResourceViewModel {

        public long Id { get; set; }
        public string Name { get; set; }

        public ResourceViewModel(Resource resource) {
            this.Id = resource.Id;
            this.Name = resource.Name;
        }

    }
}
