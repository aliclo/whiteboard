﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Whiteboard.Data;
using Whiteboard.Models.DataModels.Modules;

namespace Whiteboard.Models.ViewModels.Modules {
    public class MaterialAnalyticsViewModel {

        private const int MAX_PERCENT = 100;

        public string Name { get; set; }
        public string TotalSeen { get; set; }
        public float TotalSeenNum { get; set; }

        public List<string> StudentCommenters { get; set; }

        public List<ResourceAnalyticsViewModel> ResourceAnalytics { get; set; }

        public MaterialAnalyticsViewModel(Lecture lecture) {
            this.Name = lecture.Name;
        }

        public MaterialAnalyticsViewModel(Lecture lecture, bool anyStudents)
                : this(lecture) {

            if(!anyStudents) {
                GetResourcesNoStudents(lecture.Resources);
                TotalSeen = "No students";
                StudentCommenters = new List<string>();
            }
        }

        public async Task InitialiseAsync(AppIdentityDbContext dbContext,
                int studentsNum, Lecture lecture) {

            List<Resource> resources = lecture.Resources;

            ResourceAnalytics = new List<ResourceAnalyticsViewModel>();

            if (resources.Count() == 0) {
                TotalSeenNum = MAX_PERCENT;
                TotalSeen = "No resources";
            } else {
                await GetResources(dbContext, studentsNum, resources);
            }

            await GetStudentCommenters(dbContext, lecture);
        }

        private async Task GetResources(AppIdentityDbContext dbContext,
            int studentsNum, List<Resource> resources) {

            foreach (Resource resource in resources) {
                ResourceAnalyticsViewModel resourceAnalytic =
                    new ResourceAnalyticsViewModel(resource);

                await resourceAnalytic.InitialiseAsync(dbContext,
                    studentsNum, resource);

                TotalSeenNum += resourceAnalytic.PercentSeenNum;

                ResourceAnalytics.Add(resourceAnalytic);
            }

            TotalSeenNum = TotalSeenNum / resources.Count();
            TotalSeen = TotalSeenNum.ToString("n2") + "%";
        }

        private void GetResourcesNoStudents(List<Resource> resources) {
            ResourceAnalytics = new List<ResourceAnalyticsViewModel>();

            foreach (Resource resource in resources) {
                ResourceAnalyticsViewModel resourceAnalytic =
                    new ResourceAnalyticsViewModel(resource, false);

                ResourceAnalytics.Add(resourceAnalytic);
            }
        }

        private async Task GetStudentCommenters(AppIdentityDbContext dbContext,
                Lecture lecture) {

            StudentCommenters = await dbContext.Students.Where(s =>
                lecture.MaterialMessageBoard.Messages.Any(
                m => s.Id.Equals(m.Sender.Id))).Select(m => m.UserName)
                .ToListAsync();
        }

    }
}
