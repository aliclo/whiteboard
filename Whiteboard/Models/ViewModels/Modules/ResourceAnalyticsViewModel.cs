﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Whiteboard.Data;
using Whiteboard.Models.DataModels.Accounts;
using Whiteboard.Models.DataModels.Modules;

namespace Whiteboard.Models.ViewModels.Modules {
    public class ResourceAnalyticsViewModel {

        private const int MAX_PERCENT = 100;

        public string Name { get; set; }
        public string PercentSeen { get; set; }
        public float PercentSeenNum { get; set; }

        public ResourceAnalyticsViewModel(Resource resource) {
            this.Name = resource.Name;
        }

        public ResourceAnalyticsViewModel(Resource resource,
                bool anyStudents) : this(resource) {
            
            if(!anyStudents) {
                PercentSeen = "No students";
            }
        }

        public async Task InitialiseAsync(AppIdentityDbContext dbContext,
                int studentsNum, Resource resource) {

            int amountSeen = await dbContext.UsersSeenResources.Where(
                usr => usr.ModuleCode.Equals(resource.ModuleCode)
                && usr.MaterialId.Equals(resource.MaterialId)
                && usr.ResourceId.Equals(resource.Id)).CountAsync();

            PercentSeenNum = MAX_PERCENT * ((float)amountSeen / studentsNum);

            PercentSeen = PercentSeenNum.ToString("n2") + "%";
        }

    }
}
