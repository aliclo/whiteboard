﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Whiteboard.Models.DataModels.Modules;
using Whiteboard.Models.ViewModels.Messaging;

namespace Whiteboard.Models.ViewModels.Modules {
    public class LectureViewModel {

        public long Id { get; set; }
        public string Name { get; set; }

        public List<ResourceViewModel> Resources { get; set; }

        public MessageBoardViewModel MaterialMessageBoard { get; set; }

        public LectureViewModel(JavaScriptEncoder jsEncoder,
                HtmlEncoder htmlEncoder, Lecture lecture) {

            this.Id = lecture.Id;
            this.Name = lecture.Name;

            Resources = new List<ResourceViewModel>();

            foreach (Resource resource in lecture.Resources) {
                Resources.Add(new ResourceViewModel(resource));
            }

            this.MaterialMessageBoard = new MessageBoardViewModel(jsEncoder,
                htmlEncoder, lecture.MaterialMessageBoard);
        }

    }
}
