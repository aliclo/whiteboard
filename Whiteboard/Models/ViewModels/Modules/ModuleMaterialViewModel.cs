﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Whiteboard.Models.DataModels.Modules;

namespace Whiteboard.Models.ViewModels.Modules {
    public class ModuleMaterialViewModel : IAccessDisplayable {

        public string Code { get; set; }
        public List<LectureViewModel> Lectures { get; set; }

        private readonly List<AccessDisplay> _accessDisplays;

        public ModuleMaterialViewModel(JavaScriptEncoder jsEncoder,
                HtmlEncoder htmlEncoder, Module module,
                List<AccessDisplay> accessDisplays) {

            Code = module.Code;
            Lectures = new List<LectureViewModel>();

            foreach (Lecture lecture in module.Lectures) {
                Lectures.Add(new LectureViewModel(
                    jsEncoder, htmlEncoder, lecture));
            }

            this._accessDisplays = accessDisplays;
        }

        public List<AccessDisplay> GetAccessDisplays() {
            return _accessDisplays;
        }

    }
}
