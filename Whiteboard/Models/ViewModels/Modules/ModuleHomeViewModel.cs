﻿using System.Text.Encodings.Web;
using Whiteboard.Models.ViewModels.Messaging;
using Whiteboard.Models.DataModels.Modules;
using System.Collections.Generic;

namespace Whiteboard.Models.ViewModels.Modules {
    public class ModuleHomeViewModel : IAccessDisplayable {

        public string Code { get; set; }
        public string Name { get; set; }
        public MessageBoardViewModel ModuleMessageBoard { get; set; }

        private readonly List<AccessDisplay> _accessDisplays;

        public ModuleHomeViewModel(JavaScriptEncoder jsEncoder,
                HtmlEncoder htmlEncoder, Module module,
                List<AccessDisplay> accessDisplays) {

            this.Code = module.Code;
            this.Name = module.Name;
            this.ModuleMessageBoard = new MessageBoardViewModel(jsEncoder,
                htmlEncoder, module.ModuleMessageBoard);

            this._accessDisplays = accessDisplays;
        }

        public List<AccessDisplay> GetAccessDisplays() {
            return _accessDisplays;
        }

    }
}
