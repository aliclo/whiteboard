﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Whiteboard.Models.DataModels.Modules;

namespace Whiteboard.Models.ViewModels.Modules {
    public class AddedMaterialViewModel {

        public long MaterialId { get; set; }
        public long MaterialMessageBoardId { get; set; }

        public AddedMaterialViewModel(Lecture material) {
            MaterialId = material.Id;
            MaterialMessageBoardId = material.MaterialMessageBoardId;
        }

    }
}
