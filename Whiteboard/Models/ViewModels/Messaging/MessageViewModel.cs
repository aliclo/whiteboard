﻿using System.Text.Encodings.Web;
using Whiteboard.Models.DataModels.Messaging;

namespace Whiteboard.Models.ViewModels.Messaging {
    public class MessageViewModel {

        public string Contents { get; set; }

        public MessageViewModel(JavaScriptEncoder jsEncoder,
                HtmlEncoder htmlEncoder, Message message) {

            Contents = message.GetDisplay(jsEncoder, htmlEncoder);
        }

    }
}
