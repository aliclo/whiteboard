﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Whiteboard.Models.DataModels.Messaging;

namespace Whiteboard.Models.ViewModels.Messaging {
    public class MessageBoardViewModel {

        public long Id { get; set; }
        public List<MessageViewModel> Messages { get; set; }

        public MessageBoardViewModel(JavaScriptEncoder jsEncoder,
                HtmlEncoder htmlEncoder, MessageBoard messageBoard) {

            this.Id = messageBoard.Id;

            Messages = new List<MessageViewModel>();

            foreach(Message message in messageBoard.Messages) {
                Messages.Add(new MessageViewModel(
                    jsEncoder, htmlEncoder, message));
            }
        }

    }
}
