﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Whiteboard.Data;
using Whiteboard.Models.DataModels.Accounts;
using Whiteboard.Models.JoinModels;

namespace Whiteboard.Models.DataModels.Messaging {
    public abstract class Message {

        public long Id { get; set; }

        public string SenderId { get; set; }

        public AppUser Sender { get; set; }

        public DateTime DateTimeSent { get; set; }

        public long MessageBoardId { get; set; }

        [NotMapped]
        public MessageBoard MessageBoard { get; set; }

        public Message() {

        }

        public Message(AppUser sender, DateTime dateTimeSent) {
            this.SenderId = sender.Id;
            this.Sender = sender;
            this.DateTimeSent = dateTimeSent;
        }

        public void RegisterToMessageBoard(
                AppIdentityDbContext appIdentityDbContext,
                MessageBoard messageBoard) {

            this.MessageBoardId = messageBoard.Id;
            this.MessageBoard = messageBoard;

            long largestId = appIdentityDbContext.Messages.Where(
                m => m.MessageBoardId == MessageBoardId).Select(m => m.Id)
                .DefaultIfEmpty(0).Max();

            Id = largestId + 1;
        }

        public string GetDisplay(JavaScriptEncoder jsEncoder,
                HtmlEncoder htmlEncoder) {

            string username = 
                jsEncoder.Encode(htmlEncoder.Encode(Sender.UserName));

            string header = "<b class='msgheader'>" + username + "</b><br>";

            string footer = "<i class='msgfooter'><br>"
                + DateTimeSent.ToShortTimeString() + "</i><br>";

            string contents = GetContents(jsEncoder, htmlEncoder);

            return "<p class='message'>" + header + contents + footer + "</p>";
        }

        public async Task InitialiseAsync(AppIdentityDbContext dbContext) {
            Sender = await dbContext.AppUsers.SingleAsync(
                u => u.Id.Equals(SenderId));

            MessageBoard = await dbContext.MessageBoards.SingleAsync(
                mb => mb.Id.Equals(MessageBoardId));
        }

        public abstract string GetContents(JavaScriptEncoder jsEncoder,
            HtmlEncoder htmlEncoder);

    }
}
