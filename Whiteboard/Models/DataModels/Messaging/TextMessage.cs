﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Whiteboard.Data;
using Whiteboard.Models.DataModels.Accounts;

namespace Whiteboard.Models.DataModels.Messaging {
    public class TextMessage : Message {

        public string Text { get; set; }

        public TextMessage() : base() {

        }

        public TextMessage(AppUser sender, DateTime dateTimeSent, string text)
                : base(sender, dateTimeSent) {

            this.Text = text;
        }

        public static string ToContents(string text,
                JavaScriptEncoder jsEncoder, HtmlEncoder htmlEncoder) {

            string encoded = jsEncoder.Encode(htmlEncoder.Encode(text));

            return encoded.Replace("&#xA;", "<br/>");
        }

        public override string GetContents(JavaScriptEncoder jsEncoder,
                HtmlEncoder htmlEncoder) {

            return ToContents(Text, jsEncoder, htmlEncoder);
        }

    }
}
