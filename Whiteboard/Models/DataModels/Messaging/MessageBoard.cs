﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Whiteboard.Data;
using Whiteboard.Models.DataModels.Accounts;
using Whiteboard.Models.JoinModels;

namespace Whiteboard.Models.DataModels.Messaging {
    public class MessageBoard {

        [Key]
        public long Id { get; set; }

        [NotMapped]
        public List<AppUser> Members { get; set; }

        [NotMapped]
        public List<Message> Messages { get; set; }

        public MessageBoard() {
            Members = new List<AppUser>();
            Messages = new List<Message>();
        }

        public async Task RemoveReferences(AppIdentityDbContext dbContext) {
            await PartialUpdateMessages(dbContext);

            foreach (Message message in Messages) {
                dbContext.Messages.Remove(message);
            }
        }

        public async Task InitialiseAsync(AppIdentityDbContext dbContext) {
            await UpdateMembers(dbContext);

            await FullUpdateMessages(dbContext);
        }

        private async Task UpdateMembers(AppIdentityDbContext dbContext) {
            IQueryable<MsgBoardMember> msgBoardMembers =
                dbContext.MsgBoardMembers.Where(
                    mbm => mbm.MessageBoardId.Equals(Id));

            Members = await dbContext.AppUsers.Where(u => msgBoardMembers.Any(
                mbm => mbm.MemberId.Equals(u.Id))).ToListAsync();
        }

        private async Task PartialUpdateMessages(
                AppIdentityDbContext dbContext) {

            Messages = await dbContext.Messages.Where(
                m => m.MessageBoardId.Equals(Id)).ToListAsync();
        }

        private async Task FullUpdateMessages(AppIdentityDbContext dbContext) {
            await PartialUpdateMessages(dbContext);

            foreach (Message message in Messages) {
                await message.InitialiseAsync(dbContext);
            }
        }

    }
}
