﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Whiteboard.Data;
using Whiteboard.Models.DataModels.Accounts;
using Whiteboard.Models.DataModels.Messaging;
using Whiteboard.Models.JoinModels;
using Whiteboard.Repositories;

namespace Whiteboard.Models.DataModels.Modules {
    public class Module {

        public bool Seeded { get; set; } = false;

        [Key]
        public string Code { get; set; }

        public string Name { get; set; }

        [NotMapped]
        public List<AppUser> Users { get; set; }

        [NotMapped]
        public MessageBoard ModuleMessageBoard { get; set; }

        [NotMapped]
        public List<Lecture> Lectures { get; set; }

        public Module() {

        }

        public Module(string code, string name) : this() {

            this.Code = code;
            this.Name = name;
            Users = new List<AppUser>();
            Lectures = new List<Lecture>();
        }

        public void AddUser(AppUser user) {
            Users.Add(user);
            ModuleMessageBoard.Members.Add(user);
        }

        public async Task InitialiseAsync(AppIdentityDbContext dbContext) {
            await PartialUpdateUsers(dbContext);
            await FullUpdateMessageBoard(dbContext);
            await FullUpdateLectures(dbContext);
        }

        public async Task RemoveReferences(AppIdentityDbContext dbContext) {
            await PartialUpdateMessageBoard(dbContext);
            await ModuleMessageBoard.RemoveReferences(dbContext);
            dbContext.MessageBoards.Remove(ModuleMessageBoard);

            await PartialUpdateLectures(dbContext);
            
            foreach(Lecture lecture in Lectures) {
                await lecture.RemoveReferences(dbContext);
                dbContext.Lectures.Remove(lecture);
            }
        }

        private async Task PartialUpdateUsers(AppIdentityDbContext dbContext) {
            IQueryable<UserModule> usersModule = dbContext.UserModules.Where(
                um => um.ModuleCode.Equals(Code));

            Users = await dbContext.Users.Where(u => usersModule.Any(
                um => um.UserId.Equals(u.Id))).ToListAsync();
        }

        private async Task PartialUpdateMessageBoard(
                AppIdentityDbContext dbContext) {

            ModuleMessageBoard moduleMessageBoard =
                await dbContext.ModuleMessageBoards.SingleAsync(
                mmb => mmb.ModuleCode.Equals(Code));

            ModuleMessageBoard = await dbContext.MessageBoards.SingleAsync(
                mb => mb.Id.Equals(moduleMessageBoard.MessageBoardId));
        }

        private async Task FullUpdateMessageBoard(
                AppIdentityDbContext dbContext) {

            await PartialUpdateMessageBoard(dbContext);

            await ModuleMessageBoard.InitialiseAsync(dbContext);
        }

        private async Task PartialUpdateLectures(
            AppIdentityDbContext dbContext) {

            Lectures = await dbContext.Lectures.Where(
                l => l.ModuleCode.Equals(Code)).ToListAsync();
        }

        private async Task FullUpdateLectures(
                AppIdentityDbContext dbContext) {

            await PartialUpdateLectures(dbContext);
            
            foreach(Lecture lecture in Lectures) {
                await lecture.InitialiseAsync(dbContext);
            }
        }

    }
}
