﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Whiteboard.Data;

namespace Whiteboard.Models.DataModels.Modules {
    public class Resource {

        public string ModuleCode { get; set; }

        [NotMapped]
        public Module Module { get; set; }

        public long MaterialId { get; set; }

        [NotMapped]
        public Lecture Material { get; set; }

        public long Id { get; set; }

        public string Name { get; set; }

        public byte[] File { get; set; }

        public string FileContentType { get; set; }

        public string FileName { get; set; }

        public Resource() {

        }

        public Resource(string name, byte[] file, string fileContentType,
                string fileName) {

            this.Name = name;
            this.File = file;
            this.FileContentType = fileContentType;
            this.FileName = fileName;
        }

        public void RegisterToMaterial(
                AppIdentityDbContext appIdentityDbContext,
                Module module, Lecture material) {

            this.ModuleCode = module.Code;
            this.Module = module;

            this.MaterialId = material.Id;
            this.Material = material;

            long largestId = appIdentityDbContext.Resources.Where(
                r => r.ModuleCode.Equals(ModuleCode)
                && r.MaterialId.Equals(MaterialId)).Select(r => r.Id)
                .DefaultIfEmpty(0).Max();

            Id = largestId + 1;
        }

        public async Task InitialiseAsync(AppIdentityDbContext dbContext) {
            Module = await dbContext.Modules.SingleAsync(
                m => m.Code.Equals(ModuleCode));

            Material = await dbContext.Lectures.SingleAsync(
                m => m.ModuleCode.Equals(ModuleCode)
                && m.Id.Equals(MaterialId));
        }

    }
}
