﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Whiteboard.Data;
using Whiteboard.Models.DataModels.Messaging;

namespace Whiteboard.Models.DataModels.Modules {
    public class Lecture {

        public string ModuleCode { get; set; }

        [NotMapped]
        public Module Module { get; set; }

        public long Id { get; set; }

        public string Name { get; set; }

        [NotMapped]
        public List<Resource> Resources { get; set; }

        public long MaterialMessageBoardId { get; set; }

        [NotMapped]
        public MessageBoard MaterialMessageBoard { get; set; }

        public Lecture() {

        }

        public Lecture(string name) {
            this.Name = name;
        }

        public void RegisterToModule(
                AppIdentityDbContext appIdentityDbContext,
                Module module) {

            this.ModuleCode = module.Code;
            this.Module = module;

            long largestId = appIdentityDbContext.Lectures.Where(
                l => l.ModuleCode.Equals(ModuleCode)).Select(l => l.Id)
                .DefaultIfEmpty(0).Max();

            Id = largestId + 1;
        }

        public async Task InitialiseAsync(AppIdentityDbContext dbContext) {
            await PartialUpdateModule(dbContext);
            await PartialUpdateResources(dbContext);
            await FullUpdateMaterialMessageBoard(dbContext);
        }

        public async Task RemoveReferences(AppIdentityDbContext dbContext) {
            await PartialUpdateResources(dbContext);

            foreach (Resource resource in Resources) {
                dbContext.Resources.Remove(resource);
            }

            await PartialUpdateMaterialMessageBoard(dbContext);
            await MaterialMessageBoard.RemoveReferences(dbContext);
            dbContext.MessageBoards.Remove(MaterialMessageBoard);
        }

        private async Task PartialUpdateModule(
                AppIdentityDbContext dbContext) {

            Module = await dbContext.Modules.SingleAsync(
                m => m.Code.Equals(ModuleCode));
        }

        private async Task PartialUpdateResources(
                AppIdentityDbContext dbContext) {

            Resources = await dbContext.Resources.Where(
                l => l.ModuleCode.Equals(ModuleCode)
                && l.MaterialId.Equals(Id)).ToListAsync();
        }

        private async Task PartialUpdateMaterialMessageBoard(
                AppIdentityDbContext dbContext) {

            MaterialMessageBoard = await dbContext.MessageBoards.SingleAsync(
                mb => mb.Id.Equals(MaterialMessageBoardId));
        }

        private async Task FullUpdateMaterialMessageBoard(
                AppIdentityDbContext dbContext) {

            await PartialUpdateMaterialMessageBoard(dbContext);
            await MaterialMessageBoard.InitialiseAsync(dbContext);
        }

    }
}
