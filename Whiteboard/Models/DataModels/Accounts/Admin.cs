﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Whiteboard.Models.ViewModels.Accounts;
using Whiteboard.Repositories;
using Whiteboard.Models.DataModels.Modules;
using Whiteboard.Models.ViewModels.Modules;
using static Whiteboard.Claims;

namespace Whiteboard.Models.DataModels.Accounts {
    public class Admin : AppUser {

        private const string HOMEPAGE_ACTION_NAME = "AdminHome";
        private const string MATERIAL_ACTION_NAME = "Lecturer/MaterialCreate";
        private const string MODULE_INDEX_ACTION_NAME = "Lecturer/Index";

        public const string ROLE = Roles.ADMIN;

        public static readonly CLAIMS[] DEFAULT_CLAIMS =
            { CLAIMS.CAN_CREATE_STAFF,
            CLAIMS.CAN_MANAGE_EXISTING_ACCOUNTS };

        public static readonly CLAIMS[] AVAILABLE_CLAIMS =
            { CLAIMS.CAN_CREATE_STAFF,
            CLAIMS.CAN_MANAGE_EXISTING_ACCOUNTS };

        private static readonly Mapping RELEVANT_CLAIMS_MAPPING =
            GetRelevantClaimsMapping(AVAILABLE_CLAIMS);

        public string Title { get; set; }

        public Admin() : base() {

        }

        public Admin(string forename, string surname, List<Module> modules,
                string title) : base(forename, surname, modules) {

            this.Title = title;
        }

        public override Task<IActionResult> GetHomepage(
                Controller controller, ModuleRepository moduleRepository) {

            AdminStartViewModel viewModel = new AdminStartViewModel(this);

            return Task.FromResult<IActionResult>(
                controller.View(HOMEPAGE_ACTION_NAME, viewModel));
        }

        public override Task<IActionResult> GetMaterialPage(
                Controller controller, AppUserRepository appUserRepository,
                ModuleMaterialViewModel moduleMaterialViewModel) {

            return Task.FromResult<IActionResult>(controller.View(
                MATERIAL_ACTION_NAME, moduleMaterialViewModel));
        }

        public override IActionResult GetModuleIndexPage(Controller controller,
                ModuleHomeViewModel moduleHomeViewModel) {

            return controller.View(MODULE_INDEX_ACTION_NAME,
                moduleHomeViewModel);
        }

        public override Task SeenResource(ModuleRepository moduleRepository,
                Module module, Lecture material, Resource resource) {

            return Task.CompletedTask;
        }

        public override string GetRole() {
            return ROLE;
        }

        public override CLAIMS[] GetDefaultClaims() {
            return DEFAULT_CLAIMS;
        }

        public override CLAIMS[] GetAvailableClaims() {
            return AVAILABLE_CLAIMS;
        }

        public override int GetRelevantClaimsMapping(int i) {
            return RELEVANT_CLAIMS_MAPPING.Map[i];
        }

        public override int GetInvRelevantClaimsMapping(int i) {
            return RELEVANT_CLAIMS_MAPPING.InvMap[i];
        }

    }
}
