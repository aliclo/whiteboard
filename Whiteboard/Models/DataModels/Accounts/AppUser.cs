﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Whiteboard.Data;
using Whiteboard.Models.DataModels.Messaging;
using Whiteboard.Models.JoinModels;
using Whiteboard.Models.ViewModels.Accounts;
using Whiteboard.Repositories;
using Whiteboard.Models.DataModels.Modules;
using Whiteboard.Models.ViewModels.Modules;
using static Whiteboard.Claims;

namespace Whiteboard.Models.DataModels.Accounts {
    public abstract class AppUser : IdentityUser, IUser {

        public bool Seeded { get; set; } = false;
        public string Forename { get; set; }
        public string Surname { get; set; }

        [NotMapped]
        public List<Module> Modules { get; set; }

        [NotMapped]
        public List<MessageBoard> MessageBoards { get; set; }

        [NotMapped]
        public List<AccessDisplay> AccessDisplays { get; set; }

        public AppUser() {
            Modules = new List<Module>();
        }

        public AppUser(string forename, string surname) : this() {
            this.Forename = forename;
            this.Surname = surname;
        }

        public AppUser(string forename, string surname, List<Module> modules)
                : this(forename, surname) {

            Modules = modules;
        }

        public AppUser(UserRegisterViewModel model) : this() {
            UserName = model.Username;
            Forename = model.Forename;
            Surname = model.Surname;
            Email = model.Email;
        }

        protected static Mapping GetRelevantClaimsMapping(
                CLAIMS[] availableClaims) {

            int[] RelevantClaimsMapping = new int[Claims.CLAIMSARR.Length];
            int[] InvRelevantClaimsMapping = new int[availableClaims.Length];

            int currentIndex = 0;

            foreach (CLAIMS claim in availableClaims) {
                int claimNum = (int) claim;
                RelevantClaimsMapping[claimNum] = currentIndex;
                InvRelevantClaimsMapping[currentIndex] = claimNum;
                currentIndex++;
            }

            return new Mapping(RelevantClaimsMapping,
                InvRelevantClaimsMapping);
        }

        public async Task InitialiseAsync(AppIdentityDbContext dbContext,
                AppUserRepository appUserRepository) {

            await FullUpdateModules(dbContext);

            await FullUpdateMessageBoards(dbContext);

            await UpdateClaims(appUserRepository);
        }

        private async Task PartialUpdateModules(
                AppIdentityDbContext dbContext) {

            IQueryable<UserModule> usersModule = dbContext.UserModules.Where(
                um => um.UserId.Equals(Id));

            Modules = await dbContext.Modules.Where(m => usersModule.Any(
                um => um.ModuleCode.Equals(m.Code))).ToListAsync();
        }

        private async Task FullUpdateModules(AppIdentityDbContext dbContext) {
            await PartialUpdateModules(dbContext);

            foreach (Module module in Modules) {
                await module.InitialiseAsync(dbContext);
            }
        }

        private async Task PartialUpdateMessageBoards(
                AppIdentityDbContext dbContext) {

            IQueryable<MsgBoardMember> usersMessageBoards =
                dbContext.MsgBoardMembers.Where(
                mbm => mbm.MemberId.Equals(Id));

            MessageBoards = await dbContext.MessageBoards.Where(
                mb => usersMessageBoards.Any(
                mbm => mbm.MessageBoardId.Equals(mb.Id))).ToListAsync();
        }

        private async Task FullUpdateMessageBoards(
                AppIdentityDbContext dbContext) {

            await PartialUpdateMessageBoards(dbContext);

            foreach (MessageBoard messageBoard in MessageBoards) {
                await messageBoard.InitialiseAsync(dbContext);
            }
        }

        private async Task UpdateClaims(AppUserRepository appUserRepository) {
            AccessDisplays = new List<AccessDisplay>();

            bool[] claims = await appUserRepository.GetClaimsAsync(this);

            for (int i = 0; i < claims.Length; i++) {
                bool claim = claims[i];

                AddClaim(i, claim);
            }
        }

        private void AddClaim(int i, bool claim) {
            if (claim) {
                int index = GetInvRelevantClaimsMapping(i);
                AccessDisplay accessDisplay = CLAIMS_ACCESS[index];

                if (accessDisplay != null) {
                    AccessDisplays.Add(accessDisplay);
                }
            }
        }

        public async Task<IdentityResult> Update(
                AppUserRepository appUserRepository,
                ManageUserViewModel manageUserViewModel) {

            this.Forename = manageUserViewModel.Forename;
            this.Surname = manageUserViewModel.Surname;

            IdentityResult result = await appUserRepository.UpdateUserAsync(
                this);

            if(!result.Succeeded) {
                return result;
            }

            await appUserRepository.UpdateUserClaimsAsync(this,
                manageUserViewModel.Claims);

            return IdentityResult.Success;
        }

        public abstract Task<IActionResult> GetHomepage(Controller controller,
            ModuleRepository moduleRepository);

        public abstract Task<IActionResult> GetMaterialPage(
            Controller controller, AppUserRepository appUserRepository,
            ModuleMaterialViewModel moduleMaterialViewModel);

        public abstract IActionResult GetModuleIndexPage(
            Controller controller, ModuleHomeViewModel moduleHomeViewModel);

        public abstract Task SeenResource(ModuleRepository moduleRepository,
            Module module, Lecture material, Resource resource);

        public abstract string GetRole();

        public abstract CLAIMS[] GetDefaultClaims();

        public abstract CLAIMS[] GetAvailableClaims();

        public abstract int GetRelevantClaimsMapping(int i);

        public abstract int GetInvRelevantClaimsMapping(int i);

        protected class Mapping {

            public int[] Map { get; set; }
            public int[] InvMap { get; set; }

            public Mapping(int[] map, int[] invMap) {
                this.Map = map;
                this.InvMap = invMap;
            }

        }

    }
}
