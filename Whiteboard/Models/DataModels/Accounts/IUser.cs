﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Whiteboard.Repositories;
using static Whiteboard.Claims;

namespace Whiteboard.Models.DataModels.Accounts {
    interface IUser {

        Task<IActionResult> GetHomepage(Controller controller,
            ModuleRepository moduleRepository);

        string GetRole();

        CLAIMS[] GetDefaultClaims();

        CLAIMS[] GetAvailableClaims();

        int GetRelevantClaimsMapping(int i);

        int GetInvRelevantClaimsMapping(int i);

    }
}
