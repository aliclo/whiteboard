﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Whiteboard.Data;
using Whiteboard.Models.ViewModels.Accounts;
using Whiteboard.Repositories;
using Whiteboard.Models.DataModels.Modules;
using Whiteboard.Models.ViewModels.Modules;
using static Whiteboard.Claims;

namespace Whiteboard.Models.DataModels.Accounts {
    public class Lecturer : AppUser {

        private const string HOMEPAGE_ACTION_NAME = "StaffHome";

        private const string MATERIAL_VIEW_ACTION_NAME =
            "Student/MaterialView";

        private const string MATERIAL_CREATE_ACTION_NAME =
            "Lecturer/MaterialCreate";

        private const string MODULE_INDEX_ACTION_NAME = "Lecturer/Index";

        public const string ROLE = Roles.LECTURER;
        public static readonly CLAIMS[] DEFAULT_CLAIMS =
            { CLAIMS.CAN_WRITE_MATERIAL, CLAIMS.CAN_READ_ANALYTICS };

        public static readonly CLAIMS[] AVAILABLE_CLAIMS =
            { CLAIMS.CAN_WRITE_MATERIAL, CLAIMS.CAN_READ_ANALYTICS,
            CLAIMS.CAN_CREATE_STAFF };

        private static readonly Mapping RELEVANT_CLAIMS_MAPPING =
            GetRelevantClaimsMapping(AVAILABLE_CLAIMS);

        public string Title { get; set; }

        public Lecturer() : base() {

        }

        public Lecturer(string forename, string surname, List<Module> modules,
                string title) : base(forename, surname, modules) {

            this.Title = title;
        }

        public Lecturer(LecturerRegisterViewModel model) : base(model) {
            Title = model.Title;
        }

        public async override Task<IActionResult> GetHomepage(
            Controller controller, ModuleRepository moduleRepository) {

            List<Module> availModules = await moduleRepository
                .GetAvailableModulesAsync(this);

            LecturerStartViewModel viewModel = new LecturerStartViewModel(
                this, availModules);

            return controller.View(HOMEPAGE_ACTION_NAME, viewModel);
        }

        public override async Task<IActionResult> GetMaterialPage(
                Controller controller, AppUserRepository appUserRepository,
                ModuleMaterialViewModel moduleMaterialViewModel) {

            bool[] claims = await appUserRepository.GetClaimsAsync(this);

            int canWrite = (int) CLAIMS.CAN_WRITE_MATERIAL;
            int canWriteRel = GetRelevantClaimsMapping(canWrite);

            if (claims[canWriteRel]) {
                return controller.View(MATERIAL_CREATE_ACTION_NAME,
                    moduleMaterialViewModel);
            } else {
                return controller.View(MATERIAL_VIEW_ACTION_NAME,
                    moduleMaterialViewModel);
            }
        }

        public override IActionResult GetModuleIndexPage(Controller controller,
                ModuleHomeViewModel moduleHomeViewModel) {

            return controller.View(MODULE_INDEX_ACTION_NAME,
                moduleHomeViewModel);
        }

        public override Task SeenResource(ModuleRepository moduleRepository,
                Module module, Lecture material, Resource resource) {

            return Task.CompletedTask;
        }

        public override string GetRole() {
            return ROLE;
        }

        public override CLAIMS[] GetDefaultClaims() {
            return DEFAULT_CLAIMS;
        }

        public override CLAIMS[] GetAvailableClaims() {
            return AVAILABLE_CLAIMS;
        }

        public override int GetRelevantClaimsMapping(int i) {
            return RELEVANT_CLAIMS_MAPPING.Map[i];
        }

        public override int GetInvRelevantClaimsMapping(int i) {
            return RELEVANT_CLAIMS_MAPPING.InvMap[i];
        }

    }
}
