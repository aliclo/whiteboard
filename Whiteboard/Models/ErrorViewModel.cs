using System;
using System.Collections.Generic;
using Whiteboard.Models.ViewModels;

namespace Whiteboard.Models {
    public class ErrorViewModel : IAccessDisplayable {

        private readonly List<AccessDisplay> _accessDisplays;

        public string RequestId { get; set; }

        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);

        public ErrorViewModel(string RequestId,
                List<AccessDisplay> accessDisplays) {

            this.RequestId = RequestId;
            this._accessDisplays = accessDisplays;
        }

        public List<AccessDisplay> GetAccessDisplays() {
            return _accessDisplays;
        }
    }
}