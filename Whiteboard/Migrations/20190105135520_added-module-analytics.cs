﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Whiteboard.Migrations
{
    public partial class addedmoduleanalytics : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "UsersSeenResources",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    ResourceId = table.Column<long>(nullable: false),
                    ResourceModuleCode = table.Column<string>(nullable: true),
                    ResourceMaterialId = table.Column<long>(nullable: true),
                    ResourceId1 = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UsersSeenResources", x => new { x.UserId, x.ResourceId });
                    table.ForeignKey(
                        name: "FK_UsersSeenResources_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UsersSeenResources_Resources_ResourceModuleCode_ResourceMaterialId_ResourceId1",
                        columns: x => new { x.ResourceModuleCode, x.ResourceMaterialId, x.ResourceId1 },
                        principalTable: "Resources",
                        principalColumns: new[] { "ModuleCode", "MaterialId", "Id" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UsersSeenResources_ResourceModuleCode_ResourceMaterialId_ResourceId1",
                table: "UsersSeenResources",
                columns: new[] { "ResourceModuleCode", "ResourceMaterialId", "ResourceId1" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UsersSeenResources");
        }
    }
}
