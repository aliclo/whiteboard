﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Whiteboard.Migrations
{
    public partial class addedfilenametomaterialitem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FileName",
                table: "Resources",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FileName",
                table: "Resources");
        }
    }
}
