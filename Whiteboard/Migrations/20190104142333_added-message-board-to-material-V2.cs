﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Whiteboard.Migrations
{
    public partial class addedmessageboardtomaterialV2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "MaterialMessageBoardId",
                table: "Lectures",
                nullable: false,
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "MaterialMessageBoardId",
                table: "Lectures",
                nullable: false,
                oldClrType: typeof(long));
        }
    }
}
