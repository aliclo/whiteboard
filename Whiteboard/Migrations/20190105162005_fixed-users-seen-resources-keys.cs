﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Whiteboard.Migrations
{
    public partial class fixedusersseenresourceskeys : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_UsersSeenResources",
                table: "UsersSeenResources");

            migrationBuilder.AddColumn<string>(
                name: "ModuleCode",
                table: "UsersSeenResources",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<long>(
                name: "MaterialId",
                table: "UsersSeenResources",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "MaterialId1",
                table: "UsersSeenResources",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MaterialModuleCode",
                table: "UsersSeenResources",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_UsersSeenResources",
                table: "UsersSeenResources",
                columns: new[] { "UserId", "ModuleCode", "MaterialId", "ResourceId" });

            migrationBuilder.CreateIndex(
                name: "IX_UsersSeenResources_ModuleCode",
                table: "UsersSeenResources",
                column: "ModuleCode");

            migrationBuilder.CreateIndex(
                name: "IX_UsersSeenResources_MaterialModuleCode_MaterialId1",
                table: "UsersSeenResources",
                columns: new[] { "MaterialModuleCode", "MaterialId1" });

            migrationBuilder.AddForeignKey(
                name: "FK_UsersSeenResources_Modules_ModuleCode",
                table: "UsersSeenResources",
                column: "ModuleCode",
                principalTable: "Modules",
                principalColumn: "Code",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UsersSeenResources_Lectures_MaterialModuleCode_MaterialId1",
                table: "UsersSeenResources",
                columns: new[] { "MaterialModuleCode", "MaterialId1" },
                principalTable: "Lectures",
                principalColumns: new[] { "ModuleCode", "Id" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UsersSeenResources_Modules_ModuleCode",
                table: "UsersSeenResources");

            migrationBuilder.DropForeignKey(
                name: "FK_UsersSeenResources_Lectures_MaterialModuleCode_MaterialId1",
                table: "UsersSeenResources");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UsersSeenResources",
                table: "UsersSeenResources");

            migrationBuilder.DropIndex(
                name: "IX_UsersSeenResources_ModuleCode",
                table: "UsersSeenResources");

            migrationBuilder.DropIndex(
                name: "IX_UsersSeenResources_MaterialModuleCode_MaterialId1",
                table: "UsersSeenResources");

            migrationBuilder.DropColumn(
                name: "ModuleCode",
                table: "UsersSeenResources");

            migrationBuilder.DropColumn(
                name: "MaterialId",
                table: "UsersSeenResources");

            migrationBuilder.DropColumn(
                name: "MaterialId1",
                table: "UsersSeenResources");

            migrationBuilder.DropColumn(
                name: "MaterialModuleCode",
                table: "UsersSeenResources");

            migrationBuilder.AddPrimaryKey(
                name: "PK_UsersSeenResources",
                table: "UsersSeenResources",
                columns: new[] { "UserId", "ResourceId" });
        }
    }
}
