﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Whiteboard.Migrations
{
    public partial class removedunneededusermessagejointable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserMessages");

            migrationBuilder.AddColumn<string>(
                name: "SenderId",
                table: "Messages",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SenderId",
                table: "Messages");

            migrationBuilder.CreateTable(
                name: "UserMessages",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    MessageId = table.Column<long>(nullable: false),
                    MessageBoardId = table.Column<long>(nullable: false),
                    MessageBoardId1 = table.Column<long>(nullable: true),
                    MessageId1 = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserMessages", x => new { x.UserId, x.MessageId, x.MessageBoardId });
                    table.ForeignKey(
                        name: "FK_UserMessages_MessageBoards_MessageBoardId",
                        column: x => x.MessageBoardId,
                        principalTable: "MessageBoards",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserMessages_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserMessages_Messages_MessageId1_MessageBoardId1",
                        columns: x => new { x.MessageId1, x.MessageBoardId1 },
                        principalTable: "Messages",
                        principalColumns: new[] { "Id", "MessageBoardId" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserMessages_MessageBoardId",
                table: "UserMessages",
                column: "MessageBoardId");

            migrationBuilder.CreateIndex(
                name: "IX_UserMessages_MessageId1_MessageBoardId1",
                table: "UserMessages",
                columns: new[] { "MessageId1", "MessageBoardId1" });
        }
    }
}
