﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Whiteboard.Migrations
{
    public partial class usersandmessagesonetomanyV3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Messages_MessageBoardId",
                table: "Messages",
                column: "MessageBoardId");

            migrationBuilder.AddForeignKey(
                name: "FK_Messages_MessageBoards_MessageBoardId",
                table: "Messages",
                column: "MessageBoardId",
                principalTable: "MessageBoards",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Messages_MessageBoards_MessageBoardId",
                table: "Messages");

            migrationBuilder.DropIndex(
                name: "IX_Messages_MessageBoardId",
                table: "Messages");
        }
    }
}
