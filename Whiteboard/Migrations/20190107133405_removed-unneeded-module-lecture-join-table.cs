﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Whiteboard.Migrations
{
    public partial class removedunneededmodulelecturejointable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ModuleLectures");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ModuleLectures",
                columns: table => new
                {
                    ModuleCode = table.Column<string>(nullable: false),
                    LectureId = table.Column<long>(nullable: false),
                    LectureId1 = table.Column<long>(nullable: true),
                    LectureModuleCode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ModuleLectures", x => new { x.ModuleCode, x.LectureId });
                    table.ForeignKey(
                        name: "FK_ModuleLectures_Modules_ModuleCode",
                        column: x => x.ModuleCode,
                        principalTable: "Modules",
                        principalColumn: "Code",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ModuleLectures_Lectures_LectureModuleCode_LectureId1",
                        columns: x => new { x.LectureModuleCode, x.LectureId1 },
                        principalTable: "Lectures",
                        principalColumns: new[] { "ModuleCode", "Id" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ModuleLectures_LectureModuleCode_LectureId1",
                table: "ModuleLectures",
                columns: new[] { "LectureModuleCode", "LectureId1" });
        }
    }
}
