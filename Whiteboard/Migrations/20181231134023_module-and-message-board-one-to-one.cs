﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Whiteboard.Migrations
{
    public partial class moduleandmessageboardonetoone : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Modules_MessageBoards_ModuleMessageBoardId",
                table: "Modules");

            migrationBuilder.DropIndex(
                name: "IX_Modules_ModuleMessageBoardId",
                table: "Modules");

            migrationBuilder.DropColumn(
                name: "ModuleMessageBoardId",
                table: "Modules");

            migrationBuilder.CreateTable(
                name: "ModuleMessageBoards",
                columns: table => new
                {
                    ModuleCode = table.Column<string>(nullable: false),
                    MessageBoardId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ModuleMessageBoards", x => new { x.MessageBoardId, x.ModuleCode });
                    table.ForeignKey(
                        name: "FK_ModuleMessageBoards_MessageBoards_MessageBoardId",
                        column: x => x.MessageBoardId,
                        principalTable: "MessageBoards",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ModuleMessageBoards_Modules_ModuleCode",
                        column: x => x.ModuleCode,
                        principalTable: "Modules",
                        principalColumn: "Code",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ModuleMessageBoards_ModuleCode",
                table: "ModuleMessageBoards",
                column: "ModuleCode");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ModuleMessageBoards");

            migrationBuilder.AddColumn<long>(
                name: "ModuleMessageBoardId",
                table: "Modules",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateIndex(
                name: "IX_Modules_ModuleMessageBoardId",
                table: "Modules",
                column: "ModuleMessageBoardId");

            migrationBuilder.AddForeignKey(
                name: "FK_Modules_MessageBoards_ModuleMessageBoardId",
                table: "Modules",
                column: "ModuleMessageBoardId",
                principalTable: "MessageBoards",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
