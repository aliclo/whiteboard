﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Whiteboard.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Whiteboard.Models.DataModels.Accounts;
using System;

namespace Whiteboard {
    public class Startup {

        private const int LOCKOUT_MINUTES = 5;
        private const int LOGIN_ATTEMPTS = 3;

        public Startup(IConfiguration configuration) {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime.
        // Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services) {
            services.Configure<CookiePolicyOptions>(options => {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddDbContext<AppIdentityDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));

            LockoutOptions lockoutOptions = new LockoutOptions() {
                AllowedForNewUsers = true,
                DefaultLockoutTimeSpan = TimeSpan.FromMinutes(LOCKOUT_MINUTES),
                MaxFailedAccessAttempts = LOGIN_ATTEMPTS
            };

            services.AddIdentity<AppUser,IdentityRole>(
                options => options.Lockout = lockoutOptions)
                .AddEntityFrameworkStores<AppIdentityDbContext>()
                .AddDefaultTokenProviders();

            services.AddAuthorization(options => {

                foreach (string policy in Claims.CLAIMSARR) {
                    options.AddPolicy(policy, p => p.RequireClaim(policy));
                }

            });

            services.ConfigureApplicationCookie(
                options => options.LoginPath = "/Account/Login");

            services.AddMvc().SetCompatibilityVersion(
                CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime.
        // Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env,
                UserManager<AppUser> userManager,
                SignInManager<AppUser> signInManager,
                RoleManager<IdentityRole> roleManager,
                AppIdentityDbContext appDbContext) {

            AppUserDBInitializer.CreateRoles(roleManager);

            //AppUserDBInitializer.RemoveSeededEntries(userManager);
            //AppDBInitializer.RemoveSeededEntries(appDbContext);

            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();

                AppUserDBInitializer.AddSeededEntries(userManager);
                AppDBInitializer.AddSeededEntries(appDbContext, userManager,
                    signInManager);
            } else {
                /* 
                 * Refresh seeded entries, also ensures there are no
                 * backdoors (user's shouldn't be able to log in with
                 * the seeded accounts). Removes only the entries that
                 * have the seeded flag.
                 * 
                 * The seeded flag column can be removed if seeding will
                 * not be used later on.
                 */
                //AppUserDBInitializer.RemoveSeededEntries(userManager);
                //AppDBInitializer.RemoveSeededEntries(appDbContext);

                AppUserDBInitializer.AddSeededEntries(userManager);
                AppDBInitializer.AddSeededEntries(appDbContext, userManager,
                    signInManager);

                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseAuthentication();

            app.UseMvc(routes => {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
