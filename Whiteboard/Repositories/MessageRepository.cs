﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Whiteboard.Data;
using Whiteboard.Models.DataModels.Modules;
using Whiteboard.Models.DataModels.Accounts;
using Whiteboard.Models.DataModels.Messaging;
using Whiteboard.Models.JoinModels;

namespace Whiteboard.Repositories {
    public class MessageRepository : GeneralRepository<MessageBoard> {

        public MessageRepository(AppIdentityDbContext dbContext)
            : base(dbContext) {

        }

        public async Task<MessageBoard> CreateModuleMessageBoardAsync(
                Module module) {

            MessageBoard messageBoard = new MessageBoard();

            await _dbContext.MessageBoards.AddAsync(messageBoard);

            ModuleMessageBoard moduleMessageBoard = new ModuleMessageBoard(
                module, messageBoard);

            await _dbContext.ModuleMessageBoards.AddAsync(moduleMessageBoard);

            await _dbContext.SaveChangesAsync();

            return messageBoard;
        }

        public async Task<MessageBoard> CreateMessageBoardAsync() {
            MessageBoard messageBoard = new MessageBoard();

            await _dbContext.MessageBoards.AddAsync(messageBoard);
            await _dbContext.SaveChangesAsync();

            return messageBoard;
        }

        public async Task<MessageBoard> GetMessageBoardAsync(int id) {
            return await _dbContext.MessageBoards.SingleAsync(
                mb => mb.Id.Equals(id));
        }

        public async Task AddMessageAsync(AppUser user, Message message,
                MessageBoard messageBoard) {

            message.RegisterToMessageBoard(_dbContext, messageBoard);

            await _dbContext.Messages.AddAsync(message);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<bool> IsMemberAsync(AppUser user,
                MessageBoard messageBoard) {

            return await _dbContext.MsgBoardMembers.AnyAsync(
                mbm => mbm.MemberId.Equals(user.Id)
                && mbm.MessageBoardId.Equals(messageBoard.Id));
        }

    }
}
