﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Whiteboard.Data;
using Whiteboard.Models.DataModels.Accounts;
using Whiteboard.Models.DataModels.Messaging;
using Whiteboard.Models.DataModels.Modules;
using Whiteboard.Models.JoinModels;

namespace Whiteboard.Repositories {
    public class ModuleRepository : GeneralRepository<Module> {

        private MessageRepository _messageRepository;

        public ModuleRepository(AppIdentityDbContext dbContext)
            : base(dbContext) {

            _messageRepository = new MessageRepository(dbContext);
        }

        public async Task<bool> SeedNonExistModuleAsync(Module module) {

            Module existing = await _dbContext.Modules
                .SingleOrDefaultAsync(m => m.Code == module.Code);

            if (existing == null) {
                module.Seeded = true;

                await CreateModuleAsync(module);

                return true;
            }

            return false;
        }

        public async Task CreateModuleAsync(Module module) {
            await _dbContext.Modules.AddAsync(module);

            module.ModuleMessageBoard = await _messageRepository
                .CreateModuleMessageBoardAsync(module);
        }

        public async Task<List<Module>> GetAvailableModulesAsync(
                AppUser user) {

            List<Module> availModules = _dbContext.Modules.Where(
                m => !user.Modules.Any(sm => m.Code == sm.Code)).ToList();

            foreach(Module module in availModules) {
                await module.InitialiseAsync(_dbContext);
            }

            return availModules;
        }

        public async Task<Module> GetModuleAsync(string moduleCode) {
            Module module = await _dbContext.Modules.SingleOrDefaultAsync(
                m => m.Code.Equals(moduleCode));

            if (module == null) {
                return null;
            }

            await module.InitialiseAsync(_dbContext);

            return module;
        }

        public async Task<bool> HasAccessToModuleAsync(AppUser user,
                Module module) {

            return await _dbContext.UserModules.AnyAsync(
                um => um.UserId.Equals(user.Id)
                && um.ModuleCode.Equals(module.Code));
        }

        public async Task AddMaterialAsync(Module module, Lecture lecture) {
            lecture.RegisterToModule(_dbContext, module);
            module.Lectures.Add(lecture);

            MessageBoard messageBoard =
                await _messageRepository.CreateMessageBoardAsync();

            lecture.MaterialMessageBoard = messageBoard;
            lecture.MaterialMessageBoardId = messageBoard.Id;

            await _dbContext.Lectures.AddAsync(lecture);

            foreach (AppUser user in module.Users) {
                MsgBoardMember msgBoardMember = new MsgBoardMember(
                    messageBoard, user);

                await _dbContext.MsgBoardMembers.AddAsync(msgBoardMember);
            }

            await _dbContext.SaveChangesAsync();
        }

        public async Task<Lecture> GetMaterialAsync(string moduleCode,
                long materialId) {

            Lecture lecture = await _dbContext.Lectures.SingleAsync(
                l => l.ModuleCode.Equals(moduleCode)
                && l.Id.Equals(materialId));

            if (lecture == null) {
                return null;
            }

            await lecture.InitialiseAsync(_dbContext);

            return lecture;
        }

        public async Task AddResourceAsync(Module module, Lecture lecture,
                Resource resource) {

            resource.RegisterToMaterial(_dbContext, module, lecture);
            lecture.Resources.Add(resource);

            await _dbContext.Resources.AddAsync(resource);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<Resource> GetResourceAsync(string moduleCode,
                long materialId, long resourceId) {

            Resource resource = await _dbContext.Resources.SingleAsync(
                r => r.ModuleCode.Equals(moduleCode)
                && r.MaterialId.Equals(materialId)
                && r.Id.Equals(resourceId));

            if (resource == null) {
                return null;
            }

            await resource.InitialiseAsync(_dbContext);

            return resource;
        }

        public async Task UserSeenResourceAsync(AppUser user, Module module,
                Lecture material, Resource resource) {

            bool exist = _dbContext.UsersSeenResources.Any(
                usr => usr.UserId.Equals(user.Id)
                && usr.ModuleCode.Equals(module.Code)
                && usr.MaterialId.Equals(material.Id)
                && usr.ResourceId.Equals(resource.Id));

            if (!exist) {
                UserSeenResource userSeenResource =
                    new UserSeenResource(user, module, material, resource);

                await _dbContext.UsersSeenResources.AddAsync(userSeenResource);
                await _dbContext.SaveChangesAsync();
            }
        }

    }
}
