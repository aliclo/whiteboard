﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Whiteboard.Repositories {
    public interface IRepository<E> {

        E Get(int id);
        Task<E> GetAsync(int id);
        void Add(E e);
        void Delete(E e);

    }
}
