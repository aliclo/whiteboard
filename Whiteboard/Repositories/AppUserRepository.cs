﻿using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using System.Threading.Tasks;
using Whiteboard.Data;
using Whiteboard.Models.DataModels.Modules;
using Whiteboard.Models.DataModels.Accounts;
using Whiteboard.Models.JoinModels;
using System.Collections.Generic;
using static Whiteboard.Claims;

namespace Whiteboard.Repositories {
    public class AppUserRepository {

        private readonly UserManager<AppUser> _userManager;
        private readonly SignInManager<AppUser> _signInManager;
        private readonly AppIdentityDbContext _dbContext;

        public AppUserRepository(AppIdentityDbContext dbContext,
                UserManager<AppUser> userManager,
                SignInManager<AppUser> signInManager) {

            _dbContext = dbContext;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        public static async Task CreateNonExistUserAsync(
                UserManager<AppUser> userManager, AppUser user,
                string password) {

            AppUser userExists = await userManager.FindByNameAsync(
                user.UserName);

            if (userExists == null) {
                await CreateUserAsync(userManager, user, password);
            }
        }

        public static async Task<IdentityResult> CreateUserAsync(
                UserManager<AppUser> userManager, AppUser user,
                string password) {

            IdentityResult result = await userManager.CreateAsync(
                user, password);

            if(!result.Succeeded) {
                return result;
            }

            result = await userManager.AddToRoleAsync(user, user.GetRole());

            foreach (CLAIMS claim in user.GetDefaultClaims()) {
                await AddClaimAsync(userManager, user, claim);
            }

            return IdentityResult.Success;
        }

        public static async Task<IdentityResult> AddClaimAsync(
                UserManager<AppUser> userManager, AppUser user, string claim) {

            return await userManager.AddClaimAsync(user, new Claim(claim, ""));
        }

        public static async Task<IdentityResult> AddClaimAsync(
                UserManager<AppUser> userManager, AppUser user, CLAIMS claim) {

            string claimStr = GetClaimStr(claim);
            return await AddClaimAsync(userManager, user, claimStr);
        }

        public async Task<IdentityResult> CreateUserAsync(AppUser user,
                string password) {

            return await CreateUserAsync(_userManager, user, password);
        }

        public async Task<IdentityResult> AddClaimAsync(
                AppUser user, string claim) {

            return await AddClaimAsync(_userManager, user, claim);
        }

        public async Task<IdentityResult> AddClaimAsync(
                AppUser user, CLAIMS claim) {

            return await AddClaimAsync(_userManager, user, claim);
        }

        public async Task<IdentityResult> RemoveClaimAsync(AppUser user,
                Claim claim) {

            return await _userManager.RemoveClaimAsync(user, claim);
        }

        public async Task<IdentityResult> UpdateUserAsync(AppUser user) {
            return await _userManager.UpdateAsync(user);
        }

        public async Task UpdateUserClaimsAsync(AppUser user, bool[] claims) {
            Claim[] currentClaims = await GetClaimsMapAsync(user);

            for(int i = 0; i < claims.Length; i++) {
                Claim currentClaim = currentClaims[i];
                bool claim = claims[i];

                if(currentClaim == null && claim) {
                    int index = user.GetInvRelevantClaimsMapping(i);
                    CLAIMS c = (CLAIMS) index;
                    await AddClaimAsync(user, c);
                } else if(currentClaim != null && !claim) {
                    await RemoveClaimAsync(user, currentClaim);
                }
            }
        }

        public Task<SignInResult> SignInAsync(string username,
                string password, bool remember) {

            return _signInManager.PasswordSignInAsync(
                username, password, remember, true);
        }

        public Task SignInAsync(AppUser user, bool remember) {
            return _signInManager.SignInAsync(user, remember);
        }

        public async Task<AppUser> GetAsync(ClaimsPrincipal user) {
            AppUser appUser = await _userManager.GetUserAsync(user);

            if (appUser == null) {
                return null;
            }

            await appUser.InitialiseAsync(_dbContext, this);
            return appUser;
        }

        public async Task<AppUser> GetFromNameAsync(string name) {
            AppUser appUser = await _userManager.FindByNameAsync(name);

            if(appUser == null) {
                return null;
            }

            await appUser.InitialiseAsync(_dbContext, this);
            return appUser;
        }

        public Task<IdentityResult> AddToRoleAsync(AppUser user, string role) {
            return _userManager.AddToRoleAsync(user, role);
        }

        public Task<bool> IsInRoleAsync(AppUser user, string role) {
            return _userManager.IsInRoleAsync(user, role);
        }

        public Task SignOutAsync() {
            return _signInManager.SignOutAsync();
        }

        public async Task AddModuleAsync(AppUser user, Module module) {
            user.Modules.Add(module);
            module.AddUser(user);

            UserModule userModule = new UserModule(user, module);
            await _dbContext.UserModules.AddAsync(userModule);

            MsgBoardMember msgBoardMember = new MsgBoardMember(
                module.ModuleMessageBoard, user);

            await _dbContext.MsgBoardMembers.AddAsync(msgBoardMember);

            foreach (Lecture lecture in module.Lectures) {
                msgBoardMember = new MsgBoardMember(
                    lecture.MaterialMessageBoard, user);

                await _dbContext.MsgBoardMembers.AddAsync(msgBoardMember);
            }

            await _dbContext.SaveChangesAsync();
        }

        public async Task<bool[]> GetClaimsAsync(AppUser user) {
            IList<Claim> claimsList = await _userManager.GetClaimsAsync(user);

            bool[] claimsArr = new bool[user.GetAvailableClaims().Length];

            foreach (Claim claim in claimsList) {
                int index = (int) CLAIMS_ORDERING[claim.Type];
                int mappedIndex = user.GetRelevantClaimsMapping(index);
                claimsArr[mappedIndex] = true;
            }

            return claimsArr;
        }

        public async Task<Claim[]> GetClaimsMapAsync(AppUser user) {
            IList<Claim> claimsList = await _userManager.GetClaimsAsync(user);

            Claim[] claimsArr = new Claim[user.GetAvailableClaims().Length];

            foreach (Claim claim in claimsList) {
                int index = (int) CLAIMS_ORDERING[claim.Type];
                int mappedIndex = user.GetRelevantClaimsMapping(index);
                claimsArr[mappedIndex] = claim;
            }

            return claimsArr;
        }

        public async Task<IdentityResult> RemoveUserAsync(AppUser user) {
            return await _userManager.DeleteAsync(user);
        }

        public async Task<bool> IsLockedOutAsync(string username) {
            AppUser user = await GetFromNameAsync(username);

            if(user == null) {
                return false;
            }

            return await _userManager.IsLockedOutAsync(user);
        }

    }
}
