﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Whiteboard.Data;

namespace Whiteboard.Repositories {

    public class GeneralRepository<T> : IRepository<T> where T : class {

        protected readonly AppIdentityDbContext _dbContext;

        public GeneralRepository(AppIdentityDbContext dbContext) {
            _dbContext = dbContext;
        }

        public void Add(T e) {
            _dbContext.Set<T>().Add(e);
        }

        public void Delete(T e) {
            _dbContext.Set<T>().Remove(e);
        }

        public T Get(int id) {
            return _dbContext.Set<T>().Find(id);
        }

        public Task<T> GetAsync(int id) {
            return _dbContext.Set<T>().FindAsync(id);
        }

    }

}
